#include <stdio.h>

int swap(int *a, int *b){
  int c = a[0];
  a[0]  = b[0];
  b[0]  = c;
  return 0;
  
}

int main(int argc, char *argv[]){

  int a = 5;
  int b = 7;

  printf("Vor dem Vertauschen\n");
  printf("Wert1: %d, Wert2: %d\n",a,b);
  swap(&a,&b);
  printf("Nach dem Vertauschen\n");
  printf("Wert1: %d, Wert2: %d\n",a,b); 
  return 0;
  
}
