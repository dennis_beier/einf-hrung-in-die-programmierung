//Grenzen für Ganzzahlen
#include <limits.h>
// Grenzen für Gleitkomma
#include <float.h>
//malloc
#include <stdlib.h>
//printf
#include <stdio.h>
#include <string.h>
#define output stdout
#define NUMBER_DATATYPES 4

typedef long int integer_limit;
typedef long double float_limit;

typedef enum enum_types {integer = 0, floatingpoint =1} enum_types;
typedef union datatype_limit {
    integer_limit int_limit;
    float_limit   flt_limit;
} datatype_limit;

typedef struct struct_datatype_limit {
    char datatype_name[15];
    datatype_limit maximal_limit, minimal_limit;
    enum_types type;
    int maxlength;
} struct_datatype_limit;


int set_max_and_min(struct_datatype_limit *a,datatype_limit min,datatype_limit max, enum_types type) {

    if(type == integer) {
        a->type = integer;
        a->maximal_limit.int_limit = max.int_limit;
        a->minimal_limit.int_limit = min.int_limit;
    } else if (type == floatingpoint) {
        a->type = floatingpoint;
        a->maximal_limit.flt_limit = max.flt_limit;
        a->minimal_limit.flt_limit = min.flt_limit;
    }
    return 0;
}

struct_datatype_limit *new_datatype(datatype_limit min, datatype_limit max, char* datatype_name, enum_types type) {
    static struct_datatype_limit *a =0;
    static unsigned int current_index = 0;
    if (  a == 0) {
        a = malloc(sizeof(struct_datatype_limit) * NUMBER_DATATYPES);
        set_max_and_min(&a[0], min,max,type);
    } else {
        set_max_and_min(&a[current_index], min,max,type);
    }
    //printf("%s %i\n", datatype_name, current_index);
    strncpy(a[current_index].datatype_name,datatype_name,15);
    current_index += 1;
    return a;
}

int print_datatypse(struct_datatype_limit *a) {
    for(int i = 0; i<NUMBER_DATATYPES; i +=1) {
        printf("\nName: %s\n", a[i].datatype_name);
        if(a[i].type == integer) {
            printf("Minimum %ld\n", a[i].minimal_limit.int_limit);
            printf("Maximum %ld\n", a[i].maximal_limit.int_limit);
        } else if(a[i].type == floatingpoint) {
            printf("Minimum %Le\n", a[i].minimal_limit.flt_limit);
            printf("Maximum %Le\n", a[i].maximal_limit.flt_limit);
        }
    }
    return 0;
}


int main (int argc, char *argv[]) {

    datatype_limit float_max 		= {.flt_limit = FLT_MAX};
    datatype_limit float_min 		= {.flt_limit = FLT_MIN};
    datatype_limit integer_max   	= {.int_limit = INT_MAX};
    datatype_limit integer_min 		= {.int_limit = INT_MIN};
    datatype_limit short_min   		= {.int_limit = SHRT_MIN};
    datatype_limit short_max 		= {.int_limit = SHRT_MAX};
    datatype_limit double_max 		= {.flt_limit = DBL_MAX};
    datatype_limit double_min 		= {.flt_limit = DBL_MIN};
    struct_datatype_limit *a;
    enum_types type;

	new_datatype(float_min,float_max,   "float", type = floatingpoint);
	new_datatype(double_min,double_max, "double", type = floatingpoint);
	new_datatype(integer_min, integer_max, "int", type= integer);
    a = new_datatype(short_min,short_max, "short", type =integer);
    print_datatypse(a);
    return 0;

}
