int print_menu(void){
  clear_screen();
  printf("Möchten sie\n");
  printf("1. Einen neuen Datensatz anlegen\n");
  printf("2. Einen bestehenden Datensatz löschen\n");
  printf("3. Alle bestehenden Daten anzeigen\n");
  printf("O: Zum beenden");
  printf("\n");
  //Scanf lässt eine Leerzeile stehen
  int choice = strtol(read_string("Geben sie ihre Wahl ein\n"),NULL,10);
  printf("Ihre Wahl war %d \n",choice);
  return choice;
}

int print_data_menu(void){
  clear_screen();
  printf("Möchten sie\n");
  printf("1. Den Vornamen ändern\n");
  printf("2. Den Nachnamen ändern\n");
  printf("3. Das Alter ändern\n");
  //Scanf lässt das Neue Zeile Zeichen stehen
  int choice = strtol(read_string("Geben sie ihre Wahl ein\n"),NULL,10);
  printf("Ihre Wahl war %d \n",choice);
  return choice;
}

int file_menu(void){
  printf("1. Datei laden\n");
  printf("2. Datei neue Datei anlegen\n");
  int choice = strtol(read_string("Geben sie ihre Wahl ein\n"),NULL,10);
  printf("Ihre Wahl war %d\n",choice);
  return choice;
}
//Hauptschleife, wird aber erst als zwietes angezeiegt
void main_loop(char *filename){
  int choice,choice2;
  person *data;
  do{
    FILE *file = fopen(filename,"r");
    linked_list *list = load_offset_list(file);
    fclose(file);
    choice = print_menu();
    //Unterscheidet die Fälle nach Eingabe
    switch(choice){
      case 1:{
	//Fall1 neuen datensatz
	data = read_person();
	add_data(filename, data);
	break;
      }
      case 2:{
	int offset = 0;
	choice2 = strtol(read_string("Geben sie die zu löschende ID ein\n"),NULL,10);
	offset = get_offset_by_id(filename, list,choice2);
	printf("zu löschenden offset %d\n",offset);
	delete_by_offset(filename,offset);
// 	list = load_offset_list(file);
	while(getchar() != '\n');
	break;
      }
      case 3:{
	print_data_from_offset_list(filename,list);
	while(getchar() != '\n');
	break;
      }
    }
  }while(choice != 0);
}