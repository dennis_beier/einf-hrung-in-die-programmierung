#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "dynamic_string_file.h"
#include "clear_screen.h"
#include "dynamic_string.h"
#include "linked_list.h"
#include "person_data.h"
#include "person_list.h"
#include "offset.h"
#include "menu.h"



int main(void){
  int choice = file_menu();
  char *filename;
  linked_list *list;
  filename = read_string("Geben sie den Dateinamen ein\n");
  person *data;
  if(choice == 1){
   FILE *file = fopen(filename,"r");
   list = load_offset_list(file);
   fclose(file);
   clear_screen();
   print_list(list->first);
   printf("%d\n",get_last_id(filename));
  }else if(choice == 2){
    FILE *file = fopen(filename,"w+");
    printf("Geben sie die Daten der ersten Person ein\n");
    data = read_person();
    fclose(file);
    add_data(filename, data);
  }
  main_loop(filename);
  return 0;
}