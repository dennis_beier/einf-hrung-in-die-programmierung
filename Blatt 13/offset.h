linked_list* load_offset_list(FILE *file){
  linked_list *list = malloc(sizeof(linked_list));
  int i = 0;
  printf("\n");
  int error = 0;
  int end = 0;
  //Parst im Prinzip das File verwirft allerdings die Eingaben großteils
  do{
    long int seeker = ftell(file);
    for(i =0; i<4;i++){
    char *string = read_word_file(file);
    //printf("%s\n",string);
      if(string[0] == '\0'){
	if(i == 0){
	  end = 1;
	}else{
	  error = 1;
        }
	break;
      }else{
	end =0;
      }
      
    }
  //Steuerflag falls irgendein Fehler auftritt
   if(error){
     //printf("Error\n");
     return NULL;
   };
   //Anweisungen für Dateiende, verhindert dass das letzte Ergebnis in die Linked List geschrieben wird
   if(end == 1){
     return list;
  }
  //fügt der linked List den offser hinzu
   add_int_node_to_list(list, seeker);
 }while(true);
    return list;
}

person *get(char *filename, int offset){
  FILE *file = fopen(filename, "r");
  if(file == NULL){
    //printf("Datei nicht gefunden\n");
    return NULL;
  }
  person *data = malloc(sizeof(person));;
  fseek(file,offset,SEEK_SET);
  //ähnlicher Alghoritmus wie immer, nur hier fängt er bei einme Offset an und liest die nächsten 4 Wörter aus und 
  //schreibt sie an die richtige Stelle in einer struct
  for(int i=1;i<5;i++){
  
    char *string = read_word_file(file);
    //printf("%s\n",string);
    if(string[0] == '\0'){
	break;
    }
    
    //printf("data: %ld\n",data);
    if(i==1){
    data->surname = string;
    //printf("vorname: %s\n",data->surname);
    }
    if(i==2){
      data->last_name = string;
    }
    if(i==3){
      data->age = strtol(string,NULL,10);
    }
    if(i==4){
      data->id = strtol(string,NULL,10);
       //node* new = new_person_node(data);
//     printf("\nnode:\n\n");
//     print_person_node(new);
//     printf("\nlist\n\n");    
    }
  }
  fclose(file);
  return data;
}


void print_data_from_offset_list(char *filename, linked_list *list){
  //geht die Schleife durch und gibt jeden Knoten aus  
  for(node* current_node = list->first; current_node != NULL; current_node= next(current_node)){
	person *data = get(filename, *(int*)current_node->content);
	print_person_data(data);
	free(data);
    }
    
}

int get_last_id(char* filename){
  int id=0;
  int i = 1;
  // Parst das Dokument, merkt sich aber nur immer den letzen Eintrag
  //verwirft alles außer der ID
  FILE *file = fopen(filename,"r");
  person *data = malloc(sizeof(person));
  do{
     //printf("id zähler %d\n",i);
      //zum zählen der eigaben
     if(i == 5){
       i = 1;
      // data = malloc(sizeof(person));
     }
    char *string = read_word_file(file);
    //printf("%s\n",string);
    if(string[0] == '\0'){
      //zero_count++;
      //if(zero_count ==2)
	break;
    }
    
    //printf("data: %ld\n",data);
    if(i==1){
    data->surname = string;
    //printf("vorname: %s\n",data->surname);
    }
    if(i==2){
      data->last_name = string;
    }
    if(i==3){
      data->age = strtol(string,NULL,10);
    }
    if(i==4){
      data->id = strtol(string,NULL,10);
    
    }
    i++;
   
    }while(true);
    id = data->id;
    free(data);
    fclose(file);
    return id;
}
//Fügt einer Datei eine Person struct hinzu
void add_data(char *filename,person *data){
  //neue ID bestimmen
  data->id = get_last_id(filename)+1;
  FILE *file = fopen(filename,"a");
  //hineinschreiben
  fprintf(file, "%s %s %i %i\n",data->surname, data->last_name, data->age,data->id);
  fclose(file);

}
//Liest im prinzip zwei Teile einer Datei, die bestehen aus der ganzen Datei minus
//dem INhalt zwischen Start und Stop, leert die Datei beim öffnen und schreibt die beiden Sachen zurück
void cut_from_file(char* filename, int start, int stop){
  char *part1,*part2;
  
  part2 = read_n_chars(filename,stop,0);
  if(stop != 0){
    part1 = read_n_chars(filename,0,start);
    printf("part1\n%s\n part2:\n",part1);
  }
  FILE *file = fopen(filename,"w");
  if(stop != 0)
    fprintf(file,part1);
  
  printf(part2);
  fprintf(file,part2);
  fclose(file);
}
//Löscht von einem offset ausgehend der am anfang einer Zeile sein bis zum Ender einer Zeile
//entspricht dem löschen eines Datums
void delete_by_offset(char* filename, int offset){
  char *temp;
  int i;
  //maximale Länge von 300 Zeichen um endlosschleifen zu verhindern
  for(i =1; i<300;i++){
  temp = read_n_chars(filename, offset,i);
  printf("%s",temp);
  if(strchr(temp,'\n'))
    break;
  }
  //printf("zeilenlänge %d\n: offset %d\n",i,offset);
  cut_from_file(filename,offset,offset+i);
}
//liefert den offset einer Id aus der Datei zurück
int get_offset_by_id(char *filename,linked_list *list,int id){
  int offset=0;
  //geht alle Knoten durch und hphlt sich mittels get die Daten aus dem offset
  	for(node* current_node = list->first; current_node != NULL; current_node= next(current_node)){
	    offset = *(int*) current_node->content;
	    person *data = get(filename,offset);
	    if(data->id == id){
	      break;
	    }
	}
	return offset;
}