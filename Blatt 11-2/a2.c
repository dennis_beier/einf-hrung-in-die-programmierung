#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dynamic_string.h"

//Konstanten für die Ausgabe
const char* first_name_text = "Bitte vorname eingeben: ";
const char* second_name_text = "Bitte nachname eingeben: ";

//Personen strucktur
typedef struct person {
	char* first_name;
	char* second_name;

} person;

//Liefert zeiger auf intialieserte Personenstrucktur zurück
person* new_person() {
	//Anfordern des Speichers
	person* new_tmp_person = malloc( sizeof( person ) );
	//Falls Speicher bekommen, Standartwerte setzten
	if ( new_tmp_person != NULL )  {
		new_tmp_person->first_name = NULL;
		new_tmp_person->second_name = NULL;
	}
	return new_tmp_person;
}

// Gibt speicher einer Person frei
void free_person( person* freeing_person ) {
	if ( freeing_person != NULL ) {
		free( freeing_person->first_name );
		free( freeing_person->second_name );
		free( freeing_person );
	} 
}

#ifndef unsave_setter

//setzt neuen vornamen
void set_first_name( person* person, char* new_first_name ) {
	if ( person != NULL ) {
		free( person->first_name );
		person->first_name = new_first_name;
	}
}

//setzt neuen nachnamen
void set_second_name( person* person, char* new_second_name ) {
	if ( person != NULL ) {
		free( person->second_name );
		person->second_name = new_second_name;
	}
}

#else //spielerei wird nicht kompieliert

void set_reference( void** setting_reference, void* new_reference_value ) {
	if ( *(setting_reference) != NULL ) {
		free( *(setting_reference) );
	}
	*(setting_reference) = new_reference_value;
}

void set_reference_in_person( person* person, void** setting_reference, void* new_reference_value ) {
	if ( person != NULL ) {
		set_reference( setting_reference, new_reference_value );
	}
}

void set_first_name( person* person, char* new_first_name ) {
	if ( person != NULL ) {
		set_reference_in_person( person, (void**) &person->first_name, new_first_name );
	}
}

void set_second_name( person* person, char* new_second_name ) {
	if ( person != NULL ) {
		set_reference_in_person( person, (void**) &person->second_name, new_second_name );
	}
}

#endif

//Entfernt newline
void remove_newline( char* string ) {
	//Macht die erste stelle einer Newline ausfindig und überschriebt diese mit den Terminierungszeichen
	//Ja, dabei geht ein Byte verloren
	//Kopiert aus Stackoverflow
	char *pos;
	if ((pos=strchr(string, '\n')) != NULL) {
    	*pos = '\0';
	}
}

//Liest die Dateien einer Person ein und erstellt daraus eine Personenstrucktur
person* read_person() {
	char* first_name = read_string( first_name_text );
	char* second_name = read_string( second_name_text );

	remove_newline( first_name );
	remove_newline( second_name );

	person* person = new_person();
	set_first_name( person, first_name );
	set_second_name( person, second_name );

	return person;
}

typedef struct person_vector {
	size_t count;
	person* content;
} person_vector;

//Inizialiesiert einen Personen-Vector
person_vector* new_person_vector () {
	person_vector* new_tmp_vector = malloc( sizeof( person_vector ) );

	if ( new_tmp_vector != NULL )  {
		new_tmp_vector->count = 0;
		new_tmp_vector->content = NULL;
	}

	return new_tmp_vector;
}

//Gibt speicher wieder frei
void free_person_vector( person_vector* freeing_vector ) {
	if ( freeing_vector != NULL ) {
		free( freeing_vector->content );
		free( freeing_vector );
	}
}

//Fügt einem Vector eine Person hinzu
void add_person( person_vector* vector, person* adding_person ) {
	if( (vector != NULL) & (adding_person != NULL) ) {
		//"Erweitert" den speicher um eine Personenstrucktur
		struct person* new_tmp_content = realloc( vector->content, sizeof( person ) * (vector->count+1) );
		//Ist dies Gelungen?
		if ( new_tmp_content != NULL ) {
			//Copiert die Hinzuzufügende Person in den erweiterten Speicher
			memcpy( new_tmp_content + vector->count, adding_person, sizeof( person ) );
			vector->content = new_tmp_content;
			//Erhöt den Zähler
			vector->count = vector->count+1; 
		}
	}
}


//Vergleichfunktionen
int first_name_cmpfunc( const void* a, const void* b ) {
	return strcmp( ((person*) a)->first_name, ((person*) b)->first_name );
}

int second_name_cmpfunc( const void* a, const void* b ) {
	return strcmp( ((person*) a)->second_name, ((person*) b)->second_name );
}

int first_and_second_name_cmpfunc( const void* a, const void* b ) {
	person* pa = (person*) a;
	person* pb = (person*) b;

	int return_value = strlen(pa->first_name) - strlen(pb->first_name);

	if ( return_value == 0 ) {
		return_value = strlen(pa->second_name) - strlen(pb->second_name);
	}

	return return_value;
}

//Gibt einen Vector auf der Konsole aus
void print_vector( person_vector* vector ) {
	for( size_t index = 0 ; index < vector->count ; ++index ) {
		person* tmp_person = vector->content+index;
		printf( "%s, %s\n", tmp_person->first_name,tmp_person->second_name);
	}
} 

//Macht eine kopy eines Vectors (kein deep-copy)
//Die Personen werden kopiert, aber nicht die Referncen die die Personen selber speichern.
person_vector* copy_vector( person_vector* vector ) {
	//Neuen Vector erstellen
	person_vector* new_vector = malloc( sizeof(person_vector) );
	if ( new_vector != NULL ) {
		//Speicher für das Array(Personen) Anfordern
		person* content_copy = malloc( sizeof(person) * vector->count );
		if( content_copy != NULL ) {
			//Personen Kopieren
			memcpy( content_copy, vector->content, sizeof(person) * vector->count );
			//Das Array und die anzahl dem Neuen vector zuweisen
			new_vector->count = vector->count;
			new_vector->content = content_copy;
		} else {
			//Speicher des neuen Vectors freigeben, falls das Array(PErsonen) sich nicht kopieren lassen hat
			free( new_vector );
			new_vector = NULL;
		}
	}
	return new_vector;
}

#if __INCLUDE_LEVEL__ == 0

int main ( int argc, char** argv, char** env ) {

	person_vector* vector = new_person_vector();
	//Einlesen der Personen
	for(;;) {
		person* adding_person = read_person();
		if( strlen( adding_person->first_name ) | strlen( adding_person->second_name ) ) {
			add_person( vector, adding_person );
			free( adding_person );
		} else {
			break;
		}
	}
	//eingelesene Personen Ausgeben
	print_vector( vector );
	printf("\n");

	person_vector* vs = copy_vector( vector );
	print_vector( vs );
	printf("\n");
	//Sortieren nach Vornamen
	qsort( vs->content, vs->count, sizeof( person ), first_name_cmpfunc );

	print_vector( vs );
	free_person_vector( vs );
	printf("\n");



	vs = copy_vector( vector );
	print_vector( vs );
	printf("\n");
	//Sortieren nach Nachnamen
	qsort( vs->content, vs->count, sizeof( person ), second_name_cmpfunc );

	print_vector( vs );
	free_person_vector( vs );
	printf("\n");



	vs = copy_vector( vector );
	print_vector( vs );
	printf("\n");
	//Sortieren nach Namen-länge
	qsort( vs->content, vs->count, sizeof( person ), first_and_second_name_cmpfunc );

	print_vector( vs );
	free_person_vector( vs );
	printf("\n");
}

#endif
