#include <stdio.h>
#include <stdlib.h>
#include "random_number.h"
#include "create_array.h"
#include "print_array.h"

int main(void){
  int sizes[5];
  printf("Create Array\n");
  int **a = create_array(sizes,5);
  printf("Array created\n");
  print_array(a, sizes,5);
}