#include <stdlib.h>
#include <stdio.h>
#include "print_array.h"
#include "random_number.h"

int** init_buckets(void){
  int **bucket = malloc(sizeof(int*)*10);
  for(int i =0; i<10;i++){
    //spart das erste Malloc, weil realloc mit NUll malloc entspricht
    bucket[i] = NULL;
  }
  return bucket;
}


//Sortierschlüssel durch Module,also der eltzten Zahl
int int_sort_func(int number){
  return number%10;
}

int bucket_int(int **buckets, int *sizes,int next_value){
  int* temp;
  int sort_key = int_sort_func(next_value);
  //vergrößere Bucket
  temp = realloc(buckets[sort_key], (sizes[sort_key]+1)*sizeof(int));
  //printf("Größe %d,feld %d\n",sizes[sort_key],sort_key);
  //Bei Fehlern einen Fehlerwert zurückgeben
  if(temp == NULL){
    return EXIT_FAILURE;
    
  }
  buckets[sort_key]  = temp;
  buckets[sort_key][sizes[sort_key]] = next_value;
  sizes[sort_key]++;
  return 0;
}

int int_cmp (const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}
int main(void){
  printf("start\n");
  int bucketsize[10] = {0};
  int **buckets = init_buckets();
  //Mit zufälligen Zahlen füllen
  for(int i = 0;i<70;i++){
    int number = random_number(0,50);
    if( bucket_int(buckets,bucketsize,number) == EXIT_FAILURE){
      printf("Ram voll\n");
      break;
    }
  }
  printf("\n");
  print_array(buckets,bucketsize,10);
 
  //Buckets Sortieren
  for(int i=0;i<10;i++){
    qsort(buckets[i],bucketsize[i],sizeof(int),int_cmp);
  }
  printf("sorted\n");
  print_array(buckets,bucketsize,10);
}