#include <stdlib.h>
#include <stdio.h>
#include "random_number.h"
#include "linked_list.h"
#include "bucket_list.h"

int main(void){
 linked_list *buckets = new_linked_list();
 for(int i = 0; i<10;i++){
   add_int_bucket(buckets,i);
  }
  printf("Die erstellten Eimer\n");
  print_buckets(buckets);
   for(int i = 0; i<100;i++){
   int val = random_number(0,50);
   add_int_to_buckets(buckets,val,modulokey);
   printf("%d|",val);
  }
  printf("\n");
  node * cur = buckets ->first;
   for(int i = 0; i<10;i++){
   printf("topf %d",i);
   print_bucket_list(cur);
   printf("\n");
   cur = next(cur);
  }
  //Es sinnvoll es gleich einzusortieren, weil Linked sich in Link Lists sehr Schnell neue Daten einsortieren lassen. Im Gegensatz zum Array muss nichts kopiert werden, sondern nur die Verknüpfungen geändert werden.
  //Quicksort würde auch Sachen hin und her kopieren bzw. Dreieckstausch durch XOR, dies kann bei großen Daten sehr Lange dauern.
  //AUch kann Quicksort nicht so wirklich mit Linked Lists umgehen,man könnte die Bibliotheksfunktion nicht nutzen.
}