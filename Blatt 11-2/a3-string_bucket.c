#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "dynamic_string_file.h"

char*** init_buckets(void){
  char ***bucket = malloc(sizeof(char**)*26);
  for(int i =0; i<26;i++){
    //spart das erste Malloc, weil realloc mit NUll malloc entspricht
    bucket[i] = NULL;
  }
  return bucket;
}

int char_sort_func(char *string){
  //printf("\nchar:%hhd\n",string[0]);
  //konvertiert nach Großbuchstabe und zieht den Wert für A ab um den Platz im Alphabet zu bestimmen
  return toupper(string[0])-65;
  
}
void print_buckets(char*** buckets, int *sizes, int number_of_buckets){
  for(int i=0;i<number_of_buckets;i++){
    printf("\nbucket %d:",i);
    for(int j=0; j < sizes[i];j++){
      printf("%s ",buckets[i][j]);
    }
    
  }
  
}
//Kapsel umd strcmp
int str_cmp(const void *a, const void *b){
  return strcmp(*(char**)a,*(char**)b);
} 

int bucket_char(char ***buckets, int *sizes, char *next_value){
  char** temp;
  int sort_key = char_sort_func(next_value);
  //printf("sort key %d\n",sort_key);
  temp = realloc(buckets[sort_key], (sizes[sort_key]+1)*sizeof(char**));
  //printf("Größe %d,feld %d\n",sizes[sort_key],sort_key);
  
  //Bei Fehlern einen Fehlerwert zurückgeben
  if(temp == NULL){
    return EXIT_FAILURE;
    
  }
  buckets[sort_key]  = temp;
  buckets[sort_key][sizes[sort_key]] = next_value;
  sizes[sort_key]++;
  return 0;
}

int main(void){
  
  char ***buckets = init_buckets();
  int sizes[26] = {0};
  int number_strings =60;
  char** strings=malloc(sizeof(char*)*number_strings) ;
 

  FILE *file = fopen("bell","r");
  printf("folgender Text wird sortiert\n");
  //Lese Strings ein, siehe den Funktionen für Doku
   for(int i=0;i<number_strings;i++){
    strings[i] =  read_word_file(file);
    printf("%s ",strings[i]);
    if(char_sort_func(strings[i]) <0 || char_sort_func(strings[i]) >25){
      //printf("continue %d\n",char_sort_func(strings[i]));
      i--;
      continue;
    }
    bucket_char(buckets,sizes,strings[i]);
  }
  printf("\n\nsortierung\n");
  print_buckets(buckets, sizes, 26);
  for(int i =0; i<26;i++){
    qsort(buckets[i],sizes[i],sizeof(char*),str_cmp);
  }
  printf("\n\nAlphabetisch in den buckets sortier\n");
  print_buckets(buckets, sizes, 26);
}