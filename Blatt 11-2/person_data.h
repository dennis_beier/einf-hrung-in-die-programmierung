typedef struct person_data{
  int number;
  char **keys;
  char **values;
}person_data;

int add_key(person_data* data,char* key, char *value){
    int number_of_strings = data->number;
    if(number_of_strings == 0){
      data->keys   = malloc((sizeof(char*))*(number_of_strings+1));
      data->values = malloc( (sizeof(char*))*(number_of_strings+1));
      }else{
      data->keys   = realloc(data->keys , (sizeof(char*))*(number_of_strings+1));
      data->values = realloc(data->values, (sizeof(char*))*(number_of_strings+1));
      }
    data->keys[number_of_strings]   = key;
    data->values[number_of_strings] = value;
    data->number++;
    return 0;
}

void print_r(person_data *data){
    for(int i =0; i<data->number;i++){
            printf("print_r i:%d\n",i);
            printf("%s => %s\n",data->keys[i],data->values[i]);
    }
}
