#include <stdlib.h>
#include <stdbool.h>
typedef struct node {
	void* content;
	struct node* next,*prev;
} node;
node* next(node *node){
  //if(node->next == NULL)
    //printf("NULL ZEIGER\n");
  return node->next;
}
void print_list(node* first_node){      
  printf("\nListe:\n");
  for(node* current_node = first_node; current_node != NULL; current_node= next(current_node)){

      printf("%d|",*((int*) current_node->content));
    }
    printf("\n");
  }


//Erzeugt einen neuen 
node* new_node( void* content ) {
	node* tmp_node = malloc( sizeof(node) );
	//Wenn malloc funktioniert, dann Zeiger mit NULL initialsieren
	if( tmp_node != NULL ) {
		tmp_node->content = content;
		tmp_node->next = NULL;
		tmp_node->prev = NULL;
	}
	return tmp_node;
}

typedef struct linked_list {
	node* first;
}linked_list;


linked_list* new_linked_list(void) {
	linked_list* tmp_list = malloc( sizeof(linked_list) );
	if( tmp_list != NULL ) {
		tmp_list->first = NULL;
	}
	return tmp_list;
}

//Gibt einen Knoten mit dem Index index zurück
node* get_node( linked_list* list , size_t index ) {
	node* return_node = NULL;
	for( node* tmp_node = list->first ; tmp_node->next != NULL ; tmp_node = tmp_node->next ) {
		if( index == 0 ) {
			return_node = tmp_node;
		} else {
			--index;
		}
	}
	return return_node;
}

/*
void get_node2( linked_list* list , size_t index ) {
	node* return_node = list->first;
	if( return_node != NULL ) {
		for( size_t tmp_index=0 ;  tmp_index = index  ; ++index ) {
			if( return_node->
				return_node = return_node->next;
		}
	}
	return return_node;
}
*/

void insert_node( linked_list* list, node* inserting_node, size_t index ) {
	//Füge wenn index == 0 ganz am anfang ein sonst suche den Knoten
	if( index == 0 ) {
		node* tmp_node = list->first;
		list->first = inserting_node;
		inserting_node->next = tmp_node;
	} else {
		node* tmp_node = get_node(list, index-1 );
		inserting_node->next = tmp_node->next;
		tmp_node->next = inserting_node;
	}
}

//Vergleichsfunktion, vergleich data mit einem int wert auf Gleichheit
// int int_comp(node* node, void* data){
//   printf("%d:%d\n",*(int*)node->content, *(int*) data);
//  return *(int*)node->content == *(int*) data;
// }
// int int_gt(node* node, void* data){
//   printf("%d:%d\n",*(int*)node->content, *(int*) data);
//  return *(int*)node->content >*(int*) data;
// }

// int last_node(node* node, void* data){
//   return node->next == NULL;
// }
//sucht den letzten kleiner/gleichen Wert, falls es diesen nicht gibt, dann den letzten Knoten
int last_ngt_int(node* node, void* data){
  if(node->next == NULL){
    return true;
  }
  if( *(int*) node->next->content> *(int*) data){
    return true;
   }else{
    return false;
  }
  
}
//Suchfuntkion der eine Vergleichsfunktion übergeben werden muss
node* search(node *first_node, void* data,int cmp(node*, void* )){
  //printf("\nsearch\n");
  //Geht alle Knoeten durch bis die Vergleichsfunktion true liefert
  for(node* current_node = first_node; current_node != NULL; current_node= next(current_node)){
    if(cmp(current_node,data)){
      return current_node;
    }
  }
  return NULL;
}
//Fügt einen KNoten nach einem gegebenen Knoten ein
node* add_node_after(node *current_node,node* newnode){
  //Wenn null dann wieder letzter Knoten
  if(current_node == NULL){
      newnode->next = NULL;
  }else{
    //Sonst reiße Verbindungen auf füge sie neu zusammen
    newnode->next = current_node->next;
    current_node->next = newnode;
    newnode->prev = current_node;
    if(newnode->next != NULL){
      (newnode->next)->prev = newnode;
    }
  }
  return newnode;
}

// node* add_node_before(node *current_node,void* data){
//   node* new_node = malloc(sizeof(node));
//         new_node->content = data;
//   if(current_node == NULL){
//       new_node->next = NULL;
//   }else{
//     new_node->next = current_node;
//     if(current_node->prev != NULL){
//     current_node->prev->next = new_node;
//     }
//     new_node->prev = current_node->prev;
//     current_node->prev = new_node;
//   }
//   return new_node;
// }

//Fügt einen int Knoten in eine linked List ein 
node* add_int_node(node *first,int value){
  int *content = malloc(sizeof(int));
  *content = value;
  node* newnode = new_node(content);
  node *result = search(first,&value,last_ngt_int);
  add_node_after(result, newnode);
  return newnode;
}
//Fügt einen int KNoten in die Linked List ein, bekommt aber die linked List Datenstruktur übergeben weil
node* add_int_node_to_list(linked_list *list, int value){
   
 //Ich sonst nicht das erste Element ändern könnte wenn es null ist wird ein neuer erstellt
  if(list->first == NULL){
    //printf("noch kein Eintrag\n");
    int *content = malloc(sizeof(int));
    *content = value;
    node* newnode = new_node(content);
    list->first = newnode;
    //printf("%d\n",*(int*)newnode->content);
  }else{
     if(*(int*)list->first->content > value){
    int *content = malloc(sizeof(int));
    *content = value;
    node* newnode = new_node(content);
    insert_node(list,newnode,0);
    }else{
    return add_int_node(list->first,value);
    }
  }
  return NULL;
}
  

