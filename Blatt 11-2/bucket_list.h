typedef struct bucket_data {
	void *sortkey;
	linked_list list;
}bucket_data;
//KOnstruktor erstellt einen neuen Bucket
node* new_bucket(void *sortkey){

  bucket_data*  content= malloc(sizeof(bucket_data));
  content->sortkey = sortkey;
  content->list.first = NULL;
  return new_node(content);
}


//HIlfsfunktionen zur Ausgabe
void print_bucket(node *bucket){
      bucket_data *content = bucket->content;
      printf("%d|",*((int*) content->sortkey));
}
void print_buckets(linked_list* list){
    node*first_node = list->first;
     for(node* current_node = first_node; current_node != NULL; current_node= next(current_node)){
       print_bucket(current_node);
    }
}
void print_bucket_list(node *bucket){
  bucket_data* content = bucket->content;
  node* first = content->list.first;
  print_list(first);
}
//Einen neuen Eimer mit einem int wert als Sortierschlüssel
node *new_int_bucket(int value){
  int *key= malloc(sizeof(int));
  *key = value;
  return new_bucket(key);
}
//Sucht den letzten BUcket der einen kleiner/Gleichen Sortierschlüssel hat oder falls nicht vorhanden den letzten
int bucket_last_ngt_int(node *bucket, void* data){
  if(next(bucket) == NULL){ 
    return true;
  }
  bucket_data* content = bucket->next->content;
  int sort_key = *(int*) content->sortkey;
  //printf("key :%d|",*((int*) content->sortkey));
  if(sort_key > *(int*) data){
    //printf("key:%d | data: %d",sort_key,*(int*) data);
    return true;
  }else{
    return false;
  }
}
//sucht einen BUcket der den selben int Sortierschlüssel hat
int bucket_eq_int(node *bucket, void* data){
  bucket_data* content = bucket->content;
  int sort_key = *((int*)content->sortkey);

  if(sort_key == *(int*) data)
    return true;

  return false;

}
//Fügt einer Linked List einen Bucket hinzu
void add_int_bucket(linked_list* list,int sortkey){
  //printf("%d",sortkey);
  node*first = list->first;
  node* bucket= new_int_bucket(sortkey);
  //wenn keiner exisiterit wird first auf den neuen bucket gesetzt
  if(first == NULL){
    //printf("Kein eintrag");
    list->first= bucket;
  }else{
  //Sonst einfügen
    node* result = search(first,&sortkey,bucket_last_ngt_int);
    add_node_after(result, bucket);
  }
  
}
void add_int_to_bucket(node *bucket,int val){
  bucket_data* content = bucket->content;
  linked_list* first = &content->list;
  add_int_node_to_list(first,val);
}


int modulokey(int val){
  return val%10;
}
//Fügt einen Int wert dem jeweiligen Bucket hinzu
void add_int_to_buckets(linked_list *buckets,int value, int sortkey(int)){
  int* key = malloc(sizeof(key));
  *key  = sortkey(value);
  //printf("Sortierschlüssel %d",*key);
  node* result = search(buckets->first,key,bucket_eq_int);
  add_int_to_bucket(result,value);
  free(key);
}


