#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "random_number.h"

typedef struct number{
  int number;
}number;


int number_comp(const void* a,const void*b){
    const number *structa = a;
    const number *structb = b;
    return structa->number-structb->number;
  
}

void swap_elements(void** element1, void *element2, size_t size){
//Puffer mit der Größe eines Elementes, da 1 char = 1 Byte groß
  char buff[size];
  //print_array(buff,size);
  //Dreieckstausch mit Puffer
  memcpy(buff,element1,size);
  memcpy(element1,element2,size);
  memcpy(element2,buff,size);
}

int number_key_func(void *structa){
  return ((number*) structa)->number%10;
}

number **init_number_bucket(void){
  number **bucket = malloc(sizeof(int*)*10);
  for(int i =0; i<10;i++){
    //spart das erste Malloc, weil realloc mit NUll malloc entspricht
    bucket[i] = NULL;
  }
  return bucket;
}

int general_bucket(void** buckets,int *sizes, void* value,int size, int sort_key_func(void*)){
  void* temp;
  int sort_key = sort_key_func(value);
    //vergrößere Bucket
  temp = realloc(buckets[sort_key], (sizes[sort_key]+1)*size);
  //printf("Größe %d,feld %d\n",sizes[sort_key],sort_key);
  //Bei Fehlern einen Fehlerwert zurückgeben
  if(temp == NULL){
    return EXIT_FAILURE;
    
  }
  buckets[sort_key]  = temp;
  memcpy(((char *)buckets[sort_key])+sizes[sort_key]*size,  value,size);
  sizes[sort_key]++;
  return 0;
}

void print_number_array(number* array, int number_of_elements){
  
  for(int i = 0; i<number_of_elements;i++){
   printf("%d|",array[i].number);
  }
  
}
int main(void){
  
  number numbers[20];
  number **bucket = init_number_bucket();
  int sizes[10] = {0};
  for(int i=0; i<20;i++){
    numbers[i].number = random_number(0,50);
  }
  printf("zu sortierende Daten\n");
  print_number_array(numbers,20);
  for(int i =0; i<20; i++){
  general_bucket((void**)bucket,sizes,&numbers[i],sizeof(number),number_key_func);
  }
  printf("\nsortierte Daten\n");
   for(int i=0; i<10;i++){
    qsort(bucket[i],sizes[i],sizeof(number),number_comp);
    print_number_array(bucket[i],sizes[i]);
    printf("\n");
  }
}