#ifndef __RANDOM_NUM_H__
#define __RANDOM_NUM_H__



int random_number(int max, int min){
   double scaled = ((double) rand())/ ((double) RAND_MAX);
   return (max - min +1)*scaled + min;
}
#endif