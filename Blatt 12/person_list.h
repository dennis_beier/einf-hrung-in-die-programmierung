node* new_person_node(person *data){
  node *newnode;
  newnode = NULL;
  newnode = new_node(data);

  return newnode;
} 

node* read_person_node(void){
  person *data;
  data = read_person();
  node * newnode = new_person_node(data);
  return newnode;
}

person* get_person_data(node *node){
  return node->content;
}

int surname_cmp(node* node, void *data){
   person *person_data = get_person_data(node);
   int result = strcmp(person_data->surname,data);
   if(!result){
     return EXIT_SUCCESS;
  }
  return 0;
}

int last_name_cmp(node* node, void *data){
   person *person_data = get_person_data(node);
   int result = strcmp(person_data->last_name,data);
   if(!result){
     return 1;
  }
  return 0;
}

int id_cmp(node* node, void *data){
   person *person_data = get_person_data(node);
   if( person_data->id == *(int*) data){
     return 1;
  }else{
    return 0;
  }
  return 0;
}

void re_read_person_node(node* node, int id){
  person *person_data = get_person_data(node);
  re_read_data(person_data,id);
}

void save_node(node* cur, FILE* file){
  person* data = get_person_data(cur);
#if !defined(_double)
  fprintf(file, "%s %s %i %i\n",data->surname, data->last_name, data->age,data->id);
#elif defined(_double)
 fprintf(file, "%s %s %lf %i\n",data->surname, data->last_name, data->age,data->id);
#endif
}

node* search_for_id(linked_list *list, int id){
  return search(list->first,&id,id_cmp);
}

int search_print(node* node,void*data){
  print_person_node(node);
  return 0;
}

int search_save(node* node, void* data){
  save_node(node,data);
  return 0;
}

void save_list(linked_list *list, FILE* file){
    search(list->first,file,search_save);
}

void print_person_list(linked_list *list){
  search(list->first,NULL,search_print);
}

linked_list* load_list(FILE *file){
  linked_list *list = malloc(sizeof(linked_list));
  int i = 1;
  printf("\n");
  person *data; 
  
   do{
     
      //zum zählen der eigaben
     if(i == 5){
       i = 1;
       data = malloc(sizeof(person));
     }
    char *string = read_word_file(file);
    //printf("%s\n",string);
    if(string[0] == '\0'){
      //zero_count++;
      //if(zero_count ==2)
	break;
    }
    
    //printf("data: %ld\n",data);
    if(i==1){
    data->surname = string;
    printf("vorname: %s\n",data->surname);
    }
    if(i==2){
      data->last_name = string;
    }
    if(i==3){
      data->age = strtol(string,NULL,10);
    }
    if(i==4){
      data->id = strtol(string,NULL,10);
       node* new = new_person_node(data);
    printf("\nnode:\n\n");
    print_person_node(new);
    printf("\nlist\n\n");
    add_node(list,new);
    print_person_list(list);
    
    }
    i++;
   
    }while(true);
    return list;
  }
