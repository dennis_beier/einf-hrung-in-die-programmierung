typedef struct person{
  char *surname;
  char *last_name;
  int id; 
#if !defined(_double)
  int age;
#elif defined(_double)
  double age;
#endif
} person;

enum data_id { surname=1,last_name,age } data_id;
//liest die Daten der Personen ein
person* read_person(void){
  char* surname = read_string("Bitte Vornamen eingeben\n");
  if(surname[0] == '\n'){
    return NULL;
  }
  char *last_name = read_string("Bitte Nachnamen eingeben\n");
#if !defined(_double)
  int age= strtol(read_string("Bitte geben sie das Alter ein\n"),NULL,10);
#elif defined(_double)
  double age = strtof(read_string("Bitte geben sie das Alter ein\n"),NULL);
#endif
  person *person = malloc(sizeof(person));
  person->surname = surname;
  person->last_name = last_name;
  //id wird erst vergeben wenn es in die List geschrieben wird 
  person->age = age;
  return person;
}

void print_person_data(person* data){
  //clear_screen();
  printf("Vorname: %s\n", data->surname);
  printf("Nachname: %s\n", data->last_name);
#if !defined(_double)
  printf("Alter int: %d\n", data->age);
#elif defined(_double)
  printf("Alter double: %lf\n", data->age);
#endif
  printf("ID: %d", data->id);
  printf("\n");
}



void print_person_node(node *person_node){
  person * data = person_node->content;
  print_person_data(data);
}

void re_read_data(person* data, int data_id){
  clear_screen();
  switch (data_id)
  {
    case 1:{
      free(data->surname);
      data->surname = read_string("Geben sie einen neuen Vornamen ein\n");
      break;
    }
    case 2:{
      free(data->last_name);
      data->last_name = read_string("Geben sie einen neuen Nachnamen ein\n");
      break;
    }
    case 3:{
#if !defined(_double)
      data->age= strtol(read_string("Geben sie eine neues Alter an\n"),NULL,10);
#elif defined(_double)   
      data->age= strtol(read_string("Geben sie eine neues Alter an\n"),NULL);
#endif
      break;
    }
  } 
}
