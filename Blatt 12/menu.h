int print_menu(void){
  clear_screen();
  printf("Möchten sie\n");
  printf("1. Einen neuen Knoten anlegen\n");
  printf("2. Einen bestehenden Knoten ändern\n");
  printf("3. Einen bestehenden Knoten löschen\n");
  printf("4. Alle bestehenden Knoten anzeigen\n");
  printf("5. Datei laden\n");
  printf("6. In eine Datei Speichern\n");
  printf("O: Zum beenden");
  printf("\n");
  //Scanf lässt eine Leerzeile stehen
  int choice = strtol(read_string("Geben sie ihre Wahl ein\n"),NULL,10);
  printf("Ihre Wahl war %d \n",choice);
  return choice;
}

int print_data_menu(void){
  clear_screen();
  printf("Möchten sie\n");
  printf("1. Den Vornamen ändern\n");
  printf("2. Den Nachnamen ändern\n");
  printf("3. Das Alter ändern\n");
  //Scanf lässt das Neue Zeile Zeichen stehen
  int choice = strtol(read_string("Geben sie ihre Wahl ein\n"),NULL,10);
  printf("Ihre Wahl war %d \n",choice);
  return choice;

}

void main_loop(linked_list *list){
  int choice,choice2;
  int id;
  node* anode;
  do{
     choice = print_menu();
    switch(choice){
      case 1:{
	node* new = read_person_node();
	add_node(list,new);
	//ID vergeben
	if(new->prev == NULL){
	  printf("pre null\n");
	  get_person_data(new)->id = 1;
	}else{
	  printf("not null\n");
	 get_person_data(new)->id = get_person_data(prev(new))->id +1;
	}
	break;
      }
      case 2:{
	printf("Knoten nach ID ändern");
	id =strtol(read_string("Geben sie ihre Wahl ein\n"),NULL,10);
	anode = search(list->first,&id,id_cmp);
	if(anode == NULL){
	  printf("Knoten nicht gefunden\n");
	  while(getchar() != '\n');
	  break;
	}
	choice2 = print_data_menu();
	re_read_person_node(anode,choice2);
	break;
      }
      case 3:{
	printf("Knoten nach ID löschen");
	id =strtol(read_string("Geben sie ihre Wahl ein\n"),NULL,10);
	anode = search(list->first,&id,id_cmp);
	if(anode == NULL){
	  printf("nicht gefunden\n");
	  while(getchar() != '\n');
	}
	delete_node(list,anode);
	break;
      }
      case 4:{
      print_person_list(list);
      while(getchar() != '\n');
      break;
      }
      case 5:{
	char* filename = read_string("Datei zum laden eingeben\n");
	FILE *file =fopen(filename,"r");
	if(file == NULL){
	  printf("Datei existiert nicht");
	  while(getchar() != '\n');
	break;
	  
	}
	list = load_list(file);
	break;
      }
      case 6:{
	char* filename = read_string("Datei zum speichern eingeben\n");
	printf("file: %s\n",filename);
	FILE *file =fopen(filename,"w+");
	save_list(list,file);
	 while(getchar() != '\n');
	break;
      }
    }
  }while(choice != 0);
  
  
}