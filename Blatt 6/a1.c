#include <stdio.h>
#include <stdlib.h> 

int draw_triangle(int height){
static int drawn = 0;
static int column = 1;

  if(column <= 2*(height-1)+1){
    if(drawn < abs(height-abs(height-column))){
	printf("*");
	drawn++;
      }else {
	printf("\n");
	column++;
	drawn = 0;
      }
  draw_triangle(height);
  }
  return 0;
}

int draw_tree(int height){
  static int row = 0;
  static int drawn = 0;
  static int offset =0;
  
if(row <= height){ 
    if(row <= (6*height)/8){
	  if(drawn>(((6*height)/8) -1 - offset)){
	    if(drawn < ((6*height)/8) - offset +2*row-1){
	      printf("*");
	      drawn++;
	    }else{
	      printf("\n");
	      offset++;
	      row++;
	      drawn = 0;
	    }
	  }else{
	    drawn++;
	    printf(" ");
	    }
	}else{
	  if(drawn<(9*height/16 )+(3*height)/8){
	    if(drawn<(9*height/16 )){
	      printf(" ");
	      drawn++;
	    }else{
	      printf("*");
	      drawn++;
	    }
	  }else{
	    printf("\n");
	    drawn = 0;
	    row++;
	  }
	}
	draw_tree(height);
    }
return 0;
}

int main(int argc, char *argv[]){
  int tree_height =0;
  if(argc ==2){
    tree_height = atoi(argv[1]);
  }else{
    printf("wrong number of arguments\n");
    printf("Usage: %s tree_height\nExample: %s 25\n",argv[0],argv[0]);
    return 1;
  }
  
  draw_triangle(5);
  draw_tree(tree_height);
}