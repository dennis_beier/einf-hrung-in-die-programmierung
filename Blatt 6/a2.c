#include <stdio.h>

int prime_iterativ(int number){
  
  for(int i = 1; i< number; i++){
    int divisors = 0;
    for (int j = 2; j<i-1; j++){
      //printf("%d\n",j);
      if(!(i%j)){
//  	printf("teilbar %d %d\n",j,i);
	divisors++;
	break;
      }
    }
   if(!divisors)
     printf("%d\n",i);
  }
  return 0;
}

int is_prime_rec(int number, int index){
  if (index == 1|| number == 1){
    return 1;
  }else if(!(number%index)){
    return 0;
  }
  return is_prime_rec(number, index -1);
}

int prime_rec(int number){
  static int current_number = 1;
//   printf("prime_rec2:%d\n",current_number);

  if(current_number == number){
    return 0;
  }else if(is_prime_rec(current_number,current_number-1)){
	    printf("Primzahl %d\n", current_number);
	}
  
    current_number++;
  return prime_rec(number);
}

int main(void){
  printf("Start\n");
  prime_rec(100);
  prime_iterativ(100);
}