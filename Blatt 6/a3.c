#include <math.h>
#include <tgmath.h>
#include <stdio.h>
#include <stdlib.h>
//Meiner einer möchte darauf hinweisen, dass diese Konstante 1 zu setzen der Aufgabenstellung entspren würde
#define ARR_MAX 999

int calc_values(double max_value, double *values){
  double step = max_value/ ARR_MAX, integral;
  int i = 0;
  for(double f = 0; f<max_value && i< ARR_MAX; f+=step,i++){ 
    values[i] = fabs(sin(f)*step);
    integral += fabs(sin(f)*step);
//     printf("%lf\n",values[i]);
  }
  return 0;
}


double sum_up_iterativ(double *values){
  
//   printf("sum_up");
  double integral=0;
  for(int i = 0; i<ARR_MAX; i++){
    integral += values[i];
//     printf("%lf\n",values[i]);
//     printf("%lf\n",integral);
  }
  return integral;
}

double sum_up_recursiv(double *values){
  
  static int i = 0;
  static double integral = 0;
  if(i <ARR_MAX){
  integral += values[i];
  i++;
//   printf("%lf\n",intesgral);
  return sum_up_recursiv(values);
  }
  return integral;
}

double calc_integral_sin(double value){
  return fabs(cos(value)-ceil((value/acos(-1)))*2+1);
}

int main(int argc, char *argv[]){
  int count_lower, count_higher;
  double max = 0;
  if(argc ==2){
    max = atof(argv[1]);
  }else{
    printf("wrong number of arguments\n");
    printf("Usage: %s MAX\nExample: %s 25\n",argv[0],argv[0]);
    return 1;
  }
  double values[ARR_MAX], integral=0;
  calc_values(max, values);
  integral = sum_up_iterativ(values);
  printf("Iterative Summe:");
  printf("%lf\n", integral);
  printf("Rekursive Summe:");
  integral = sum_up_recursiv(values);
  printf("%lf\n", integral);
  printf("Um die Genaauigkeit zu erhöhen muss ich die Schritte kleiner machen, das heißt ARR_MAX vergößern oder einen kleineren Bereich berechnen lassen\n");
  double real_integral = calc_integral_sin(max);
  printf("eigentliches Integral %lf\n",real_integral);
  //Vergleich der beiden Methoden
  printf("\nBerechne Vergleich zwischen beiden Methoden\n");
  int i = 0;
  for(double f = 0.1; f< 100; f+= 0.1,i++){
    calc_values(f, values);
    integral = sum_up_iterativ(values);
    real_integral = calc_integral_sin(f);
    if(integral < real_integral){
      count_lower ++;
    }else if(integral > real_integral){
      count_higher++;
//       printf("%lf|",f);
    }
  }
  printf("Die Ergebnisse sind zu %f %% kleiner\n", ((double)(count_lower)*100)/i);
  printf("Die Ergebnisse sind zu %f %% größer\n", ((double)(count_higher)*100)/i);
  printf("Die Ergebnisse sind zu %f %% gleich\n", ((double)(i-(count_lower+count_higher))*100)/i);
  printf("Die Ergebnisse sind also tendenziell kleiner"); 
  printf("\nDie Summer rekursiv zu berechnen ist in der Praxis nicht sinnvoll, weil für jede Funktion ein eigener Stackframe ab und aufgebaut werden muss und die Aufgabe trivial mit Iteration gelöst werden kann.\n");
  printf("Die Rekursion ist also im Vergleich sehr langsam.\n");
  printf("Die for-Schleife ist die direkte Entsprechung der mathematischen Partialsumme, es macht also keinen Sinn etwas anderes zu nehmen.\n");
  
}