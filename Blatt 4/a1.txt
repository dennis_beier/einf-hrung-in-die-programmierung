Wie werden Boolsche Werte in C representiert?

Das ist ein bisschen kompliziert. 
Bolsche Ausdrücke liefern int zurück.
Im C99 Standard steht dazu folgendes oder etwas ähnliches:

"Each of the operators yields 1 if the
specified relation is true and 0 if it is false. The result has type
int. For any pair of operands, exactly one of the relations is true."

Der Typ bool oder __bool ist nicht als ein alias eines speziellen Typs definiert.
__Boll soll ein ganzzahliger Typ sein und groß genug um 0 und 1 aufzunehmen.

der typ bool ist ein Macro für _Bool, true und false integer Konstanten für 1 und 0.

Welche Werte sind true und false?

0 ist false, allen anderen true.
(Auch wenn das erst explizit im C11 Standard steht)

Finden explizite Typecasts statt?
Es finden zwei explizite typcasts statt einmal in Zeile 18 und Zeile 10.
Sie haben Einfluss auf das Ergebniss, weil in beiden Fällen die Kommastellen des umgewandelten Ausdrucks weggesschnitten werden.

Finden implizite Typecasts statt.

Ja inf folgenden Fällen:
	
	double x = sizeof(a) * 2 && 23 / printf("abc\n") ^ 4 + 1;
	
	von int nach double.
	
	double y = ((int)a / 2 + (7 | 8) << 4);
	
	Von int nach double
	
	double z = a * 6;
	con float nach double
	
	double u = 0xBADCAB1E ? 1 << 1 : 0xDEADC0DE;
	
	Von intt nach double
	
	double w = (int) (u + z * 10) / 8;
	von int nach double