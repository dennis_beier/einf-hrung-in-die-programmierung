#include <stdio.h>
#include <stdlib.h>

int main(void) {
	// Abschnitt A
	char a = 4;
	char b = 42;
	
	printf("char a %hhd\n",a);
	
	b ^= b;
	b += (a & 0x08) > 0;
	b += (a & 0x02) > 0;
	b += (a & 0x20) > 0;
	b += (a & 0x04) > 0;
	b += (a & 0x40) > 0;
	b += (a & 0x01) > 0;
	b += (a & 0x10) > 0;
	b += (a & 0x80) > 0;	
	
	printf("C: %d\n",b);
	
	// Abschnitt B
	unsigned char c =15;
	
	c = ~c + 1;
	
	printf("B: %hhd\n", c);
	
	// Abschnitt C
	// TIPP: http://en.wikipedia.org/wiki/ASCII#mediaviewer/File:ASCII_Code_Chart-Quick_ref_card.png
	
	char d = 'A'; // gueltige Werte: a-z, A-Z
	
	char x = (d & 040);
	
	printf("C: %d\n", x);
	
	char y = ~(~d | 040);
	printf("C: %c\n", y);
	
	
	return EXIT_SUCCESS;
}