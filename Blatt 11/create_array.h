#ifndef __RANDOM_NUM_H__
#include "random_number.h"
#endif


int** create_array(int* sizes, int n){
  int **array = calloc(n,sizeof(int*));
  
  for(int i =0; i<n; i++){
    sizes[i] =random_number(0,10);
    array[i] = calloc(sizes[i], sizeof(int));
    printf("%d\n", sizes[i]);
    for(int j =0; j<sizes[i]; j++){
      array[i][j] = random_number(0,10);
    }
  }
  return array;
}