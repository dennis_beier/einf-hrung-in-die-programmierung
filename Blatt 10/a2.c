#include <string.h>
#include <stdio.h>
#include <ctype.h>

void ceasar(char *c){
  //Erhöhe das Zeichen um 3
  *(c) = *(c)+3;
}

void blackened(char *c){
  //Wenn das Zeichen eine Zahl oder Nummer ist ersetze durch X, sonst mache nichts
  if(isalpha(*c) || isdigit(*c)){
    *c = 'X';
  }
}

void lower_case(char *c){
  *c = tolower(*c);
}

void manipulate_str(char *str, void (*manipulator)(char*)){
  //Wende auf alle Elemente des Arrays die übergebende Funktion an 
  for(int i =0; str[i] != '\0';i++){
   (*manipulator)(&str[i]);
  }
}

int main(void){
  char string[21] = {0};
  strcpy(string, "blackened in the air");
  manipulate_str(string,&blackened);
  printf("%s\n",string);
  strcpy(string, "blackened in the air");
  manipulate_str(string,&ceasar);
  printf("%s\n",string);
  strcpy(string, "BLACKENED IN THE AIR");
  manipulate_str(string,&lower_case);
  printf("%s\n",string);
}