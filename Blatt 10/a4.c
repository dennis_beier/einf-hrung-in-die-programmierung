#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// Zweil HIlfsfunktionen die Arrays ausgeben
int print_int_array(int *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%d|", (a[i]));
  }
  printf("\n");
  return 0;
};

int print_float_array(float *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%f|", (a[i]));
  }
  printf("\n");
  return 0;
};
int print_char_array(char *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%c|", (a[i]));
  }
  printf("\n");
  return 0;
};

//Fügt zwei Arrays zusammen, übernimmt Zeiger auf die beiden Arrays, die Anzahl der Elemente der beiden sowie die Größe in Byte des Datentyps
void* array_concat(void *arr1, void *arr2, size_t datatype_size,unsigned int size1,unsigned int size2){
  
  //Größe in Byte der beiden Arrays
  unsigned int char_size1 = size1*datatype_size;
  unsigned int char_size2 = size2*datatype_size;
  //Allokiere Arrays das so groß ist, wie beide Arrays zusammen
  char *new_array = malloc(char_size1+char_size2);
  //Ende des ersten Arrays in new_array
  char *end_of_array1 = new_array+char_size1;
  //Kopiere die beiden Arrays
  memcpy(new_array,arr1,char_size1);
  memcpy(end_of_array1,arr2,char_size2);
  return new_array;
}
int main(void){
  
  int i[]={1,2,3,4,5,6};
  int j[] ={7,8,9,10};
  int *k;
  k = array_concat(i,j,sizeof(int),6,4);
  print_int_array(k,10);
  
  float f[] ={1,2,3.5,4,6.5};
  float g[] = {1,2,3.5,4,6,5};
  float *h;
  
  char c[21];
  strcpy(c,"Controlling faster ");
  char d[17];
  strcpy(d,"obey your master");
  char *e;
  h = array_concat(f,g,sizeof(float),5,6);
  print_float_array(h,11);
  
  e = array_concat(c,d,sizeof(char),21,17);
  print_char_array(e,37);
  
  printf("Stolpersteine: Mit void Zeigern kann man nicht rechnen, sondern muss sie casten, wobei bei char sich anbietet, weil dies 1 Byte groß ist\n");
  printf("Man muss in der for Schleife mit der Datentypgröße rechnen und nicht nur i durchlaufen lassen\n");
} 