#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct multi_string{
  char** strings;
  int number_of_strings;
}multi_string;

//Anmerkung diese Funktin kann 2 Byte mehr als nötig verbrauchen
//Ist so aber viel schneller als jedes Zeichen einzeln zu reservieren
char* read_buffer(char **string, unsigned int size){
  //printf("read\n");
  const int max_buff = 4;
  char buff[max_buff];
  char *realloc_buff;
  //Der Rückgabewer wird nur nach Nullzeiger überprüft, deshalb cast nach int
  int return_value = (long int) fgets(buff,max_buff,stdin); 
  //size gibt an das wie oft sich die Funktion schon selbst aufgerufen hat bzw. das wievielfache von max_buff der String Groß ist
  if(size>1){
    //printf("realloc read buffer");
    
    realloc_buff = realloc(*(string),sizeof(char)*max_buff*size);
    //Falls kein Hauptspeicher mehr zur Verfügung steht gib einen Fehler Wert zurück, sonst wird der Puffer kopiert
    if(realloc_buff != NULL){
      *(string) = realloc_buff;
    }else{
      return NULL;
    }
    //An dem zweiten Durchlauf wird mehr Speicher für den String reserviert
  }
  strcat(*(string),buff);
  //Wenn kein EOF oder Zeilenende gefunden wurde, lese die nächsten Zeichen ein
  if(return_value != 0 && !(strchr(buff,'\n'))){
    return read_buffer(string,size+1);  
  }else{
  //Sonst gebe Zeiger auf string zurück
    return *(string);
  }
}

//Kapsel um eine rekursive Funktion
char *read_string(void){
  char *string = calloc(4,sizeof(char));
  printf("Geben sie einen String ein\n");
  return read_buffer(&string,1);
  
}

int read_multiple_strings(multi_string *multi){
  unsigned int number_of_strings = 0;
  char *str;
  char **strings = calloc(sizeof(char*),1);
  do{
    str = read_string();
   
    //Wenn der zweite string eingelesen wird, allokiere mehr Platz für die Zeiger
    if(number_of_strings > 0){
      //printf("%ld",sizeof(char*)*(number_of_strings+1));
      strings = realloc(strings, (sizeof(char*))*(number_of_strings+1));
    }
      //Schreibe Zeiger auf string in das Zeigerarray
     strings[number_of_strings] = str;
    //printf("%s",strings[number_of_strings]);
    number_of_strings++;
  }while(str[0]!='\n');
  //Schreibe daten in die Struktiur
  multi->strings = strings;
  multi->number_of_strings = number_of_strings;
  return EXIT_SUCCESS;
}


//Gibt die Struktur aus
int print_multi_string(multi_string multi){
  printf("Ausgabe\n");
  //Gehe jeden Zeiger durch und gebe die Strings aus
  for(int i =0;i<multi.number_of_strings;i++){
    if(multi.strings[i][0] != '\n'){
    printf("%s",multi.strings[i]);
    }
  }
  return EXIT_SUCCESS;
}

int main(void){
  multi_string multi;
  read_multiple_strings(&multi);
  print_multi_string(multi);
}