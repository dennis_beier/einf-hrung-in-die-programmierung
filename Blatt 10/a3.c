#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int print_array(char *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%c|", (a[i]));
  }
  printf("\n");
  return 0;
};

int print_int_array(int *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%d|", (a[i]));
  }
  printf("\n");
  return 0;
};

int print_float_array(float *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%f|", (a[i]));
  }
  printf("\n");
  return 0;
};

//Funktion zwei Speicherbereiche zu vertauschen
void swap_elements(void* element1, void *element2, size_t size){
//Puffer mit der Größe eines Elementes, da 1 char = 1 Byte groß
  char buff[size];
  //print_array(buff,size);
  //Dreieckstausch mit Puffer
  memcpy(buff,element1,size);
  memcpy(element1,element2,size);
  memcpy(element2,buff,size);
}


void array_reverse(void* ptr,int number_of_elements,size_t datatype_size){
  //Zähle bis zur Hälfte des Arrays   
  for(int i = 0; i <number_of_elements/2; i++){
    //linkes Element zum tauschen
    char *left_lement = ptr+(i*datatype_size);
    //rechtes Element zum tauschen 
    char *right_element = ptr+(number_of_elements-i-1)*datatype_size;
    //tausche beide Elemente
    swap_elements(left_lement,right_element,datatype_size);
  }
  
}



int main(void){
  
  char c[] ={'a','b','c','d','e'};
  array_reverse(&c,5,sizeof(char));
  print_array(c,6);
  int i[] = {1,2,3,4,5,6,7,8,9};
  array_reverse(i,9,sizeof(int));
  print_int_array(i,9);
  float f[] = {1,2,3,4.5,5,6};
  array_reverse(f,6,sizeof(float));
  print_float_array(f,6 );
  printf("Stolpersteine: Mit void Zeigern kann man nicht rechnen sondern muss sie casten, wobei bei char sich anbietet, weil dies 1 Byte groß ist\n");
  printf("Man muss in der for Schleife mit der Datentypgröße rechnen und nicht nur i durchlaufen lassen");
}