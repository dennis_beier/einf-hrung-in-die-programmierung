#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Konstanten für Größe und Breite der Matrix
#define mat_height 10
#define mat_width  10
// static const int mat_height = 4;
// static const int mat_width = 4;

typedef struct flood_data{
  //x,y des ersten Pixels und dessen Gewicht
  int x,y,weight;
}flood_data;

int mirror_matrix_both(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width]);
int get_random_bool(){
  return rand() % 2;
}
//liefert den zweier modulo einer zufälligen Zahl zurück
int fill_mat(char mat1[mat_height][mat_width]){
  for(int i = 0; i<mat_height; i++){
    for(int j =0; j<mat_width;j++){
      mat1[i][j]= get_random_bool();
    }
  }
  return 0;
}

//vertauscht die Reihenfolge innerhalb der Zeilen
int mirror_matrix_x(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width]){
  for(int i = 0; i<mat_height; i++){
    for(int j =0; j<mat_width;j++){
      mat2[i][(mat_width-1)-j] = mat1[i][j];
    }
  }
  return 0;
}

//vertauscht die Reihenfolge der Zeilen

int mirror_matrix_y(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width]){
  
    for(int i = 0; i<mat_height; i++){
    for(int j =0; j<mat_width;j++){
      mat2[(mat_height-1)-i][j] = mat1[i][j];
    }
  }
  return 0;
}


//Macht die Zeilen zu spalten bzw. umgekehrt
int mirror_matrix_diag(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width]){
  if(mat_height==mat_width){
  //wenn quadratisch vertausche Zeilen und spalten
      for(int i = 0; i<mat_height; i++){
	for(int j =0; j<mat_width;j++){
	  mat2[j][i] = mat1[i][j];
      }
    }
  }else{
      //Die Spiegelung an einer diagonalen entspricht einer Spiegelung an beiden achsen
     mirror_matrix_both(mat1,mat2);
  }
  return 0;
}

//Gibt eine Matrix aus
int print_matrix(char mat[mat_height][mat_width]){
  for(int i = 0; i<mat_height; i++){
    for(int j =0; j<mat_width;j++){
      printf("%hhd|",mat[i][j]);
    }
    printf("\n");
  }
  printf("\n");
  return 0;
}
int print_int_matrix(int mat[mat_height][mat_width]){
  for(int i = 0; i<mat_height; i++){
    for(int j =0; j<mat_width;j++){
      printf("%hhd|",mat[i][j]);
    }
    printf("\n");
  }
  printf("\n");
  return 0;
}
//spiegelt beide Achsen
int mirror_matrix_both(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width]){

   //Puffer
   char mat3[mat_height][mat_width];
   mirror_matrix_x(mat1,mat3);
   mirror_matrix_y(mat2,mat3);
   return 0;
}

//invertiert matrix, selbsterklärend
int invert(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width]){
    for(int i = 0; i<mat_height; i++){
    for(int j =0; j<mat_width;j++){
      mat2[i][j] = !(mat1[i][j]);
      }
    }
  return 0;
}

//rekursives flooodfill mit 8 nachbarn, völlig unsinnnig aber lustig benannt
int plan9(char mat1[mat_height][mat_width],int buff_mat[mat_height][mat_width],int i,int j,char alt,char neu){
  int dirty = 0;
  if(i>(mat_height-1) || j> (mat_width-1) || i<0 || j<0 )
    return dirty;
  
  if((mat1[i][j] == alt) && (buff_mat[i][j] == 0)){
      dirty++;
      buff_mat[i][j] = neu;
      dirty += plan9(mat1, buff_mat,i-1,j-1,1,neu);
      dirty += plan9(mat1, buff_mat,i,j-1,1,neu);
      dirty += plan9(mat1, buff_mat,i+1,j-1,1,neu);
      dirty += plan9(mat1, buff_mat,i,j+1,1,neu);
      dirty += plan9(mat1, buff_mat,i+1,j+1,1,neu);
      dirty += plan9(mat1, buff_mat,i ,j+1,1,neu);
      dirty += plan9(mat1, buff_mat,i-1,j+1,1,neu);
      dirty += plan9(mat1, buff_mat,i-1,j,1,neu);
  }
  return dirty;
}

//zeichnet das rößte Flutgbiet
int cluster(char mat1[mat_height][mat_width], int buff[mat_height][mat_width]){
    char neu = 1;
    int size;
    int flood_mat[mat_height][mat_width];
    //speichert die Daten der jeweils größten Flutung
    flood_data flood_data = {.x=0,.y=0,.weight = 0};
    for(int i = 0; i<mat_height; i++){
      for(int j =0; j<mat_width;j++){
	//wenn dirty flag gesetzt beginne neues flutgebiet
	size = plan9(mat1, flood_mat,i,j,1,neu);
	  if(size){
	      if(size > flood_data.weight){
		flood_data.x = j;
		flood_data.y = i;
	      }
	      neu++;
	  }
	}
  plan9(mat1, buff,flood_data.y,flood_data.x,1,1);
    }
  return neu;
}

// axis 1 spiegelt an der Xachse, 2 an der Y, 3 die Diagonale
int mirror(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width], int axis){
  if(axis==1){
    mirror_matrix_x(mat1,mat2);
  }else if(axis == 2){
    mirror_matrix_y(mat1,mat2);
  }else if(axis == 3){
    mirror_matrix_diag(mat1,mat2);
  }
  return 0;
}

int intersect(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width],char mat3[mat_height][mat_width]){
     for(int i = 0; i<mat_height; i++){
	for(int j =0; j<mat_width;j++){
	    if(mat1[i][j] && mat2[i][j]){
	      mat3[i][j] =1; 
	    }
	}
     }
  return 0;
}
// Union ist ein Schluesellwort ich kann keine solche Funktion definieren
int mat_union(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width],char mat3[mat_height][mat_width]){
     for(int i = 0; i<mat_height; i++){
	for(int j =0; j<mat_width;j++){
	    if(mat1[i][j] || mat2[i][j]){
	      mat3[i][j] =1; 
	    }
	}
     }
  return 0;
}

//Rotation entspricht Spiegelung an der Diagonalen mit Spiegelung an Y
//0 == 90 grad, 1==  -90 grad
int rotate(char mat1[mat_height][mat_width], char mat2[mat_height][mat_width], int  mode){
  if (mat_height != mat_width){
    printf("cant rotate");
    return 0;
  }
  char mat3[mat_height][mat_width];
  if(mode==0){
  mirror_matrix_diag(mat1, mat3);
  mirror_matrix_y(mat3, mat2);
  }else{
    for(int i = 0; i<3;i++){
      rotate(mat1,mat2,0);
    }
  }
  return 0;
}

int main(void){
  int buff_mat[mat_height][mat_width]={{0}};
  char buff_mat2[mat_height][mat_width]={{0}};
  srand((unsigned int)time(NULL));
  printf("main\n"); 
  char mat1[mat_height][mat_width];
  char mat2[mat_height][mat_width];
  fill_mat(mat1);
  printf("original\n");
  print_matrix(mat1);
  printf("mirror x\n");
  mirror_matrix_x(mat1,mat2);
//   printf("print\n");
  print_matrix(mat2);
  printf("mirror y\n");
  mirror_matrix_y(mat1,mat2);
  print_matrix(mat2);
  printf("diagonals\n");
  mirror_matrix_diag(mat1,mat2);
  print_matrix(mat2);
  printf("original\n");
  print_matrix(mat1);
  printf("floodmatrix\n");
  cluster(mat1, buff_mat);
  print_int_matrix(buff_mat);;
  invert(mat1,mat2);
  printf("invertiert\n");
  print_matrix(mat2);
  printf("intersect\n");
  intersect(mat1,mat2,buff_mat2);
  print_matrix(buff_mat2);
  printf("union\n");
  mat_union(mat1,mat2,buff_mat2);
  print_matrix(buff_mat2);
  printf("Rotation 90\n");
  rotate(mat1,mat2,0);
  print_matrix(mat2);
  printf("Rotation -90\n");
  rotate(mat1,mat2,1);
  print_matrix(mat2);
  printf("original\n");
  print_matrix(mat1);
}