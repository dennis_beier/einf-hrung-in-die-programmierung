#include <stdio.h>
#include <stdlib.h>


void mess ( int *n ) {
//inhalt von *n (myValues[3])

printf("%d\n" , *n++);
//Gibt n aus und erhöht dann n um eins,myValues[5])
printf("%d\n" , *++n ) ;
//erhöht erst n um eins und gibt dann n aus, also myValues[5]
printf("%d\n" , -3[n] ) ;
// der []-Operator steht für die Additon auf einen Zeiger, nach dem Distributivgesetz ist es also egal ob die Zahl oder der Zeiger vorn steht
//Das minus wird zuletzt ausgwertet also wird -(n+3) ausgegeben
printf("%d\n" , (-3)[n] ) ;
//Hier wird (n-3) gebildet, weil das minus vor der Addition ausgewertet wird 
printf("%d\n" , n[-6] ) ;
//Gibt das erste Element von n aus, also n[0], siehe Zaunpfahlproblem
}


 int main (void) {
 //int array myValues wird mit den den Werten 0 bis 9 initialisiert
 int myValues [] = { 0 , 1 , 2 ,3,4,5,6,7,8,9};
 //int a wird mit dem inhalt des zweiten feldes von myValues (1) befüllt
 int a = myValues [1] ;


int *p1;
//p1 wird die Adresse von myValues[0] zugewiesen
p1 = &myValues [0] ;
//dem int b wird der inhalt des Speicherbereiches auf den p1 zeigt zugewiesen 
int b = *p1;
//int c wird zugwiesen: (dereferenzierung von p1  + 5 = 0 + 5) - (dereferenzierung (pi+5) zeigt = 5) = 0
 int c = (*p1+5) - *(p1+5);
//p1 wird die adresse des Anfangs des Speicherbereichs von myValues zugewiesen
 p1 = myValues;
//int d wird Inhalt des Speicherbereichs an der adresse p1+4 zugewiesen = 4
 int d = *(p1+4);
//p1 wird die Speicheradresse des 6ten Feldes von myValues zugewiesen = 5
 p1 = &myValues [5];
//int e wird der Inhalt von myValues[5] zugewiesen = 4
 int e = myValues [5];
//int f wird der Inhalt des Speicherbereichs an der adresse p1 übergeben = 4
 int f = *p1;
//int g wird der Inhalt des Speicherbereichs an der adresse p1-2 übergeben = 3

 int g = *(p1-2);
//myValues[0] wird auf 40000 gesetzt

 myValues [0] = 40000 ;
 //int h wird der Inhalt des Speicherbereichs an der adresse p1-5 übergeben = 40000

 int h = *(p1-5);

//erklärung siehe oben
 printf("a : %d \n" , a);
 printf("b : %d \n" , b);
 printf("c : %d \n" , c);
 printf("d : %d \n" , d);
 printf("e : %d \n" , e);
 printf("f : %d \n" , f);
 printf("g : %d \n" , g);
 printf("h : %d \n" , h);
 mess(&myValues [3]);
 return EXIT_SUCCESS;
 }

