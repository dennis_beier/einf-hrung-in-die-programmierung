#include <stdio.h>

void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;

  
}

int print_array(int *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%d|", (a[i]));
  }
  printf("\n");
  return 0;
};




int qsort_partition_int(int *a, int length){
  static int pivot =0;
  int i =0;
  int j = length -1;
  print_array(a,length);
  while(1){
  //suche ein Element das kleiner gleich das Pivot Element ist
    while(a[i]<= a[pivot]){
      if(i == length){
	break;
      }
      i++;
    }
    if(i == length -1){
      }
      //Suche ein Element das größer ist
    while(a[j] >= a[pivot]){
      if(j <= 0){
	break;
      }
      j--;

    }

	  //tauschen wenn i größer j
	  if (i > j)
	    break;
	  swap(&a[i],&a[j]);
  }
  //sonst tausche pivot und j
  swap(&a[pivot],&a[j]);
  return j;
}

void qsort_int(int *arr, int len){
  int pivot = 0;
  if(len >1){
    pivot = qsort_partition_int(arr, len);
    qsort_int(arr, pivot);
    qsort_int((arr+(pivot))+1,(len-(pivot))-1);
  }
}

int main(void){
  
  int a[18] = {10,112,14,13,12,11,15,9,8,7,6,5,4,3,2,1,99,100};
    //print_array(a,5);

  //int pivot = qsort_partition_int(a,5);
    //qsort_int(a,5);
   qsort_int(a,18);
   print_array(a,18);
  
}