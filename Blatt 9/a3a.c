#include <stdlib.h>
#include <stdio.h>

int print_array(int *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%d|", (a[i]));
  }
  printf("\n");
  return 0;
};

int* binary_search(int *arr, int len, int needle){
  print_array(arr,len);
  int* result = 0;
  int* first_half = arr+(len/2);
  //wenn die Länge null ist, dann gebe den inhalt von arr zurück falls arr needle enthält sonst null
  if((len)<=0){
     if(*(arr) == needle){
      return arr;
    }else {
      return NULL;
    }
  }else{
    //wenn die an der Stelle der ersten Hälfte +-1 größer als needle ist , dann dursuche bis dahin
    if(*(first_half)>=needle){
      result = binary_search(arr,(len/2),needle);
      if(result){
	  return result;
      }
    }else{
    //Sonst durchsuche zweite Hälfte
      return binary_search(first_half+1,len,needle);
    }
  }
  return NULL;
}

int* linear_search(int* arr, int len, int needle){
  for(int i =0; i<len; i++){
    if(arr[i]== needle){
      return arr+i;
    }
  }
  return 0;
}

int main(void){
 int a[1000];
 int *result;
 for(int i=0; i<1000;i++){
    a[i] =i;
 };
  
  result = linear_search(a,100,5);
  printf("%d\n", *result);
 
}