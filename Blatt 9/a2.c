#include <stdio.h>
#include <stdlib.h>
int main(void){
  char *p[] = {
    "AUGUST",
    "SEPTEMBER",
    "OKTOBER",
    "NOVEMBER",
    "DEZEMBER"
  };
  //cp wir in der umgekehrten Reihenfolge von p befültt
  char **cp[] = {p+4,p+3,p+2,p+1,p};
  //cpp ist ein zeiger auf cp
  char ***cpp = cp;
  //v ist ein zeiger auf p
  char **v =p;
  //a wird die erste Zeile der Ersten Spalte von p zugewiesen, also A
  char a = **p;
  //b wird die erste Spalte der zweiten Zeile zugewiesen, also S
  char b = **(p+1);
  //  wird die vierte Spalte der dritten Zeile zugewiesen
  char c = *(*(p+4)+3);
  //d wird 
  char d = *(*p+4+3);
  
  printf("1 %c\n",a);
  printf("2 %c\n",b);
  printf("3 %c\n",c);
  printf("4 %c\n",d);
  
  v++;
  printf("5 %c\n",**v);
  
  v++;
  v++;
  v++;
  (*v)++;
  (*v)++;
  (*v)++;
  //v wird viermal  erhöht, und dann der Zeiger auf den V zeigt dreimal erhöht
  // steht also auf p[3][2]
  //gibt das zweite E in Dezember aus
  printf("6 %c\n",v[0][0]);
  //Gibt das zweite O in Oktober, weil es zwei Zeilen hochspringt
  printf("7 %c\n",v[-2][0]);
  //Gibt das erste E aus weil es zwei Spalten zutüvk geht
  printf("8 %c\n",v[0][-2]);
  //Gibt %s aus weil hier die Grenze von p überschritten wird und im Bereich von printf gelesen wird
  printf("%s\n", **cpp+6);
  //Ab hier keine Ausgabe sonder Segmentation fault, es wird über die Speichergrenzen des Programmes geschrieben
//   printf("10 %s\n",cpp[-1][-1]+4);
//   printf("11 %s\n",*cpp[-2]+2);
  return EXIT_SUCCESS;
}