#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stddef.h>

void swap(int *a, int *b){

  //Dreieckstausch durch XOR
  *(a) = (*a)^(*b);
  *(b) = (*a)^(*b);
  *(a) = (*a)^(*b);  
}

int get_random_number(int max, int min){
   double scaled = ((double) rand())/ ((double) RAND_MAX);
   return (max - min +1)*scaled + min;
}
//Erzeugt zufällige Indezes und vertauscht die beiden Zahlen
int rand_swap_array(int *a,int size){
  for(int i = 0; i< size; i++){
    int new_index = get_random_number(0,size-1);
//     printf("new index:%d",i);
    swap(&a[i],&a[new_index]);
  }
  return 0;
}

int unique_fill(int *a, int size){
//ffüllt das Array mit Zahlen von 1 bis tausend
  int one_to_thousand[999];
  for(int i=0;i<999;i++){
    one_to_thousand[i] = i+1;
  }
  rand_swap_array(one_to_thousand,999);
  //kopiert die ersten n Byte in das Array der Zufallszahlen
  memcpy(a,one_to_thousand,sizeof(int)*size);
  return 0;
}

int print_array(int *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%d|", a[i]);
  }
  printf("\n");
  return 0;
};

int* search(int *start, int *stop, int number){
//   char c;
  int* result = 0;
  int* first_half = (start+((stop-start)/2));
//   print_array(start,stop-start+1);
  // wenn die selbe Adresse da ist gib die Zahl oder NUll zurück
  if((stop-start)<=0){
     if(*(start) == number){
      return start;
    }else {
      return NULL;
    }
  }else{
    result = search(start,first_half,number);
     if(result){
	return result;
    }else{
      return search(first_half+1,stop,number);
    }
  }
}

int main (void){
  printf("start\n");
  srand(time(NULL));
  int a[8] = {1,2,3,4,5,6,7};
  int *result = search(&a[0],&a[6],4);
  int* result2 = search(&a[0],&a[6],3);
  print_array(a,7);
  swap(result,result2);
  print_array(a,7);
}