#include <stdio.h>
#include <tgmath.h>
#include <math.h>

int swap(long double **a, long double **b){
  //Zeiger werden nach long int gecastet und dann durch Xor im Dreieck getauscht
  //Cast notwendig, weil XOR auf Zeiger nicht anwenbar
  *(a) = (long double *)((long int)(*a)^(long int)(*b));
  *(b) = (long double *)((long int)(*a)^(long int)(*b));
  *(a) = (long double *)((long int)(*a)^(long int)(*b));
  return 0;
}

int print_array_links(long double **a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    //Anmerkung *(a)[i] funktioniert nicht
    printf("%.2Lf|", *(a[i]));
  }
  printf("\n");
  return 0;
};

int print_array(long double *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%.2Lf|", (a[i]));
  }
  printf("\n");
  return 0;
};

int main(void){
  
  long double content[100];
  long double* index[100];
  
  for(int i= 0;i<100;i++){
    int buff = 99-(i/2);
    content[i]=pow(10,buff);
    index[i] = &content[i];
  }
  
  swap(&index[5],&index[7]);
  print_array_links(index,100); 
  print_array(content,100);
}