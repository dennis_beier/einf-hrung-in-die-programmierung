#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stddef.h>

int rand_swap_array(int *a,int size);

struct array_index{
  
  int value, index;
  
}array_index;

int unique_fill(int *a, int size){
  
  int one_to_thousand[999];
  for(int i=0;i<999;i++){
    one_to_thousand[i] = i+1;
  }
  rand_swap_array(one_to_thousand,999);
  memcpy(a,one_to_thousand,sizeof(int)*size);
  return 0;
}

int get_random_number(int max, int min){
   double scaled = ((double) rand())/ ((double) RAND_MAX);
   return (max - min +1)*scaled + min;
}

//Vertauscht die Adresse zweier Zeiger
int pswap(int **a, int **b){
  //Zeiger werden nach long int gecastet und dann durch Xor im Dreieck getauscht
  //Cast notwendig, weil XOR auf Zeiger nicht anwenbar
  *(a) = (int *)((long int)(*a)^(long int)(*b));
  *(b) = (int *)((long int)(*a)^(long int)(*b));
  *(a) = (int *)((long int)(*a)^(long int)(*b));
  return 0;
}
// Tauscht den Inhalt zweier Variabelen
int swap(int *a, int *b){

  //Dreieckstausch durch XOR
  *(a) = (*a)^(*b);
  *(b) = (*a)^(*b);
  *(a) = (*a)^(*b);
  return 0;
  
}

int* max(int *array, int size){
  /**
   * Lokales Maximum
   */
  int max = 0;
  /**
   * Index des lokalen Maximums 
   */
  int max_index = 0;
  //Geht alles durch und sucht nach dem Maximum  
  for(int i = 0; i<size; i++){   
    if (array[i] > max){
      max 	 = array[i];
      max_index  = i;
    }
  }
  //Compiler sagt sonst dass ich die Variable nicht benutze, da hat er seinen Willen
  max_index = max_index;
  return &array[max_index];
}

int rand_swap_array(int *a,int size){
  for(int i = 0; i< size; i++){
    int new_index = get_random_number(0,size-1);
//     printf("new index:%d",i);
    swap(&a[i],&a[new_index]);
  }
  
  return 0;
}

int print_array(int *a,int size){
  printf("|");
  for(int i = 0; i<size; i++){
    printf("%d|", a[i]);
  }
  printf("\n");
  return 0;
};

int main (void){
  srand(time(NULL));
  int size = 25;
  int ar[size];
//   int *ptra = &ar[1];
//   int *ptrb = &ar[5];
  ptrdiff_t max_diff = 0;
  
  unique_fill(ar,size);
  int *maxp = max(ar,size);
  max_diff = (sizeof(ar)/sizeof(int))-(maxp - ar);
  printf("original:\n");
  print_array(ar,size);
  printf("ab maximalen Wert\n");
  print_array(maxp,max_diff);
//   printf("%d",(int)max_diff);
  return 0;
}