#include <stdlib.h>
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <time.h>
#include <limits.h>

// Strukture eines Punktes
typedef struct point{
  double y;
  double x;
}Point;

double get_random_number(int max, int min){
  return ((double) rand())/ ((double) RAND_MAX);
}

//Struktur die ein Array von Punkten verwaltet
typedef struct point_list_struct{
  Point *points;
  // Gesamte Anzahl der Punkte
  unsigned long int number_of_points;
  //Anzahl der aktuelle allokierten Punkte
  unsigned long int alloc_size;  
}Point_list_struct;

//initiert die Struktur zur Verwaltung der Punkte
int init_point_list_structure(Point_list_struct *Points,unsigned int number_of_points){
  
  Points->points = malloc(sizeof(Point)*number_of_points);
  //Überpürft korrekte Initalisierung, sonst wird einfach eine feste Anzahl von Blöcken immer allokiert
  if(Points->points == NULL){
	Points->points = malloc(sizeof(Point)*100000);
	Points->alloc_size = 100000;
	if(!Points->points)
	  return EXIT_FAILURE;
  }else{
    Points->alloc_size = number_of_points;
  }
  Points->number_of_points = number_of_points;
  return EXIT_SUCCESS;
}

int fill_buffer(Point_list_struct *struc){
  
 for(int i = 0; i < struc->alloc_size;i++){
   struc->points[i].x = get_random_number(1,0);
   struc->points[i].y = get_random_number(1,0);
   //printf("(%lf;%lf)\n",struc->points[i].x,struc->points[i].y);
  } 
  return EXIT_SUCCESS;
}; 

int realloc_struct_buffer(Point_list_struct *Points){
  
  free(Points->points);
  Points->points = malloc(sizeof(Point)*Points->alloc_size);
  if(Points->points == NULL){
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

int initialize_random_generator(){
  
  srand(time(NULL));
  return 0;
}

unsigned long int calc_points_in_circle(Point_list_struct *struc,long unsigned int number_of_steps){
  unsigned long int b = 0;
  for(unsigned long int i = 0, j = 0; i< struc->number_of_points; i++,j++){
    //printf("%d\n",i);
    if(j == struc->alloc_size){
      realloc_struct_buffer(struc);
      fill_buffer(struc);
      j = 0;
      }
    
    if(!(i%number_of_steps) && i){
      printf("Näherung nach %lu Schritten: %lf\n",i, (((double) b*4 / (double)i)) );
    }
    //Wurzel ziehen nicht nötig, ist die Summe größer eins ist auch Wurzel größer eins
   b += (pow((struc->points[j].x),2) + pow((struc->points[j].y),2) <= 1);
  }
return b;
}

int main (int argc, char *agrv[]){
  
  
  time_t end  = 0;
  unsigned long int number_of_points = 0;
  unsigned long int number_iteration_steps = 0;
  unsigned long int in_circle = 0;
  struct point_list_struct struc;
  initialize_random_generator();
  
  do{
    printf("Geben sie eine Zahl größer zwischen 0 und 10^8 ein.\n" );
    scanf("%lu", &number_of_points);
    
  }while(number_of_points <= 0);
   do{
    printf("Nach wie vielen Schritten soll eine Ausgabe erfolgen, 0 für die Ausgabe der Dezile.\n");
    scanf("%lu", &number_iteration_steps);
    if(number_iteration_steps == 0){
      number_iteration_steps = number_of_points /10;
      if(!number_iteration_steps)
	number_iteration_steps = 1;
    }
  }while(number_of_points <= 0);
  
  
  if(init_point_list_structure(&struc,number_of_points) == EXIT_FAILURE){
    printf("Konnte keinen Speicher allokieren\n");
    return EXIT_FAILURE;
  }
  time_t begin = time(NULL);
  fill_buffer(&struc);
  in_circle = calc_points_in_circle(&struc,number_iteration_steps);
  end = time(NULL);
  printf("Punkte im Kreis: %lu von %lu\n", in_circle,number_of_points);
  long double pi_iterate =  (((long double) in_circle*4 / (long double)number_of_points));
  printf("Näherung der Kreiszahl nach allen Wiederhlungen: %Lf\n", pi_iterate);
  printf("Abweichung von der in Bibliothek hinterlegten %Lf\n",acos(-1)/pi_iterate);
  printf("Rechenzeit %.0fs\n",difftime(end,begin));
  printf("Zeit pro Punkt: %lems\n",(double)(end-begin)/(double)number_of_points);
}