#include <stdlib.h>
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>
#include <sys/mman.h>


double get_random_number(int max, int min){
  return ((double) rand())/ ((double) RAND_MAX);
}
int initialize_random_generator(){
  
  srand(time(NULL));
  return 0;
}
int main(void){
  
  time_t end  = 0;
  unsigned long int number_of_points = 0;
  unsigned long int number_iteration_steps = 0;
  unsigned long int in_circle = 0;
  initialize_random_generator();
  double x,y;
  
  do{
    printf("Geben sie eine Zahl zwischen 0 und 10^8 ein.\n" );
    scanf("%lu", &number_of_points);
    
  }while(number_of_points <= 0);
   do{
    printf("Nach wie vielen Schritten soll eine Ausgabe erfolgen, 0 für die Ausgabe der Dezile.\n");
    scanf("%lu", &number_iteration_steps);
    if(number_iteration_steps == 0){
      number_iteration_steps = number_of_points /10;
      if(!number_iteration_steps)
	number_iteration_steps = 1;
    }
  }while(number_of_points <= 0);
  
  
  time_t begin = time(NULL);

  for(unsigned long int i=1;i<number_of_points;i++){
    x = get_random_number(1,0);
    y = get_random_number(1,0);
    in_circle += x*x + y*y <= 1;
    if(!(i%number_iteration_steps) && i){
      printf("Näherung nach %lu Schritten: %lf\n",i, (((double) in_circle*4 / (double)i)) );
    }
  }
  end =  time(NULL);
  double pi_iterate =  ((( double) in_circle*4 / (double)number_of_points));
  printf("Näherung der Kreiszahl nach allen Wiederholungen: %lf\n", pi_iterate);
  printf("Geschätzte Abweichung %lf\n",pi_iterate/acos(-1));
   printf("Rechenzeit %.0fs\n",difftime(end,begin));
  printf("Zeit pro Punkt: %lems\n",(double)(end-begin)/(double)number_of_points);
}