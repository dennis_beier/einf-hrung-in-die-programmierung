#include <math.h>
#include <stdio.h>
#include <tgmath.h> 

int main(void){

 unsigned int max_number;
 int positions;
  do{
    printf("Geben sie eine Zahl größer 0 ein.\n" );
    scanf("%u", &max_number);
  }while(max_number <= 0);
  
  for(int number=1;number<max_number;number++){
  positions = log10(number);
  int digit_sum = 0;
  int working_copy = number;
    for(int i = positions; i>=0;i--){
      digit_sum += working_copy/pow(10,i); 
      working_copy %= (int)pow(10,i);
    }
    if(!(number%digit_sum))
      printf("%d\n",number);
  }
 
}