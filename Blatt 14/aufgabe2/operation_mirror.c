#include "operation.h"
///vertauscht zwei Pixel
void swap_pixel(pixel *pix1, pixel *pix2){
	pixel buff = *pix1;
	*pix1 = *pix2;
	*pix2 = buff;
}
//Spiegelt das Bild an der X Achse nach oben
void operation(canvas *c){
  int height = c->pixmap->height;
  int width = c->pixmap->width;
  for(int i=0;i<width/2;i++){
    for(int j=0;j<height;j++){
	  //printf("swapping %d %d\n", i,j);
	   swap_pixel(canvas_get_pixel(c,i,j),canvas_get_pixel(c,width-i-1,j));
      }
   }
}