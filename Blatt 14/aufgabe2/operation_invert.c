#include "operation.h"
//Invertiert die farben
void operation(canvas *c){
  //printf("invertiere\n");
  int height = c->pixmap->height;
  int width = c->pixmap->width;
  pixel *pix;
  //printf("Invertiere Höhe: %d Breite: %d", width,height);
  for(int i=0;i<width;i++){
    for(int j=0;j<height;j++){
	   pix = canvas_get_pixel(c,i,j);
	   for(int k = 0; k<4;k++){
	      pix->colors[k] = 255 - pix->colors[k];
	   }
      }
   }
}

