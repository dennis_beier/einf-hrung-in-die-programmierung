#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "dynamic_string_file.h"
#include <inttypes.h>
#include "netpbm.h"

//initiert die Bitmap, der Datenstruktur mit dem eigentlichen Bild
bitmap *init_bitmap(int width, int height){
	pixel **bit_map = calloc(sizeof(pixel*),width);
	for(int i =0;i<width;i++){
		bit_map[i] = calloc(sizeof(pixel),height);
	}
	bitmap *strucmap = malloc(sizeof(bitmap));
	strucmap->map = bit_map;
	strucmap->width = width;
	strucmap->height = height;
	return strucmap;
}
//addiert zwei Pixel
void add_pixel(pixel *pix1, pixel *pix2){
  for(int i = 0; i<3;i++){
    pix1->colors[i] +=pix2->colors[i];
  }
}
//multipliziert einen Pixel mit einem Integer
void mult_pixel(pixel *pix1, int i){
  for(int i = 0; i<3;i++){
    pix1->colors[i] *= i;
  }
}
//Gibt einen Pixel aus
void print_pixel(pixel *pix){
#ifdef debug
  printf("%d %d %d|",pix->colors[0],pix->colors[1],pix->colors[2]);
#endif
}
//erzeigt einen Farbgradienten im ein Testbild zu erzeugen
void make_gradient(color color1, color color2, canvas *c){
    int height =   c->pixmap->height;
    int width  = 	 c->pixmap->width;
    //Füllt die Werte mit der Anfangsfarbe
    for(int i=0;i<width;i++)
      for(int j=0;j<height;j++)
	  c->pixmap->map[i][j] = color1;
      

  double step[3];
  //Berechnet einen Vektor vom Punkt Color 1 zum Punkt COlor 2
  //Also eine Gerade durch den RGB Würfel
  for(int i = 0; i<3;i++){
    step[i] = (double)(color2.colors[i]-color1.colors[i])/(double)height;
  }
  printf("%lf %lf %lf\n",step[0],step[1],step[2]);
  printf("height width %d %d\n",height,width);
  //Addiert step*Zeile zu jedem Pixel
  for(int i=0;i<width;i++){
    for(int j=0;j<height;j++){
	for(int k=0;k<3;k++){
	  c->pixmap->map[i][j].colors[k] += step[k]*i;
	}
	//print_pixel(&c->pixmap->map[i][j]);
    }
    //printf("\n");
  }
}
//Schreibt einen Pixel in eine Datei
void write_pixel(FILE *file, pixel *pix){
  //printf("write Pixel\n");
  fwrite(pix->colors, sizeof(uint8_t),3,file);
}
//SChreibt eine bitmap in eine Datei
void write_bitmap(FILE* file, bitmap *pixmap){
#ifdef debug
  printf("Schreibe\n");
#endif
  int height = pixmap->height;
  int width = pixmap->width;
  //Schleife die jeden einzelnen Pixel Schreibt
  for(int i=0;i<width;i++){
    for(int j=0;j<height;j++){
      write_pixel(file,&pixmap->map[i][j]);
      print_pixel(&pixmap->map[i][j]);
    }
#ifdef debug
    printf("\n");
#endif
  }
}
/**
* reserviert den benötigten Speicher;
* im Fehlerfall soll NULL zurückgegeben werden
*
* \param width Breite des Bildes
* \param width Hoehe des Bildes
*/
canvas *canvas_init(int width, int height){
	canvas *picture = malloc(sizeof(canvas));
	if(picture){
	picture->pixmap = init_bitmap(width , height);
	picture->magic  = "P6";
	picture->maxcolor = 255;
	}
	return picture;
}
/**
* gibt den reservierten speicher frei
*/

void canvas_free(canvas *c){
  for(int i =0;i<c->pixmap->width;i++){
    free(c->pixmap->map[i]);
  }
  free(c->pixmap->map);
  free(c->pixmap);
  free(c);
}
/**
* gibt einen Pointer auf das jeweilige pixel an der Position (x, y) zurueck
*/
pixel *canvas_get_pixel(canvas *c,int x, int y){
  return &(c->pixmap->map[x][y]);
}

//Funktioniert nicht, vieleicht später fixxen
void convert_plainmap_to_bitmap(canvas *c, uint8_t *plainmap){
  int height = c->pixmap->height;
  int width = c->pixmap->width;
  for(int i=0;i<width;i++){
    for(int j=0;j<height;j++){
	   memcpy(canvas_get_pixel(c,i,j)->colors, &(plainmap[i*(j*3)]), sizeof(uint8_t)*3);
	   //print_pixel(canvas_get_pixel(c,i,j));
      }
      //printf("\n");
  }
}


/**
* liest eine pbm Datei (wie oben beschrieben) ein und gibt den Pointer zurueck;
* im Fehlerfall soll NULL zurück gegeben werden
*
* \param fp
 der bereits lesbare FILE pointer
*/

canvas *read_pbm(FILE *fp){
  char *magic = read_word_file(fp);
  int height,width,maxcolor;
  uint8_t buff[3];
  fscanf(fp,"%d %d",&width, &height);
  fscanf(fp,"%d",&maxcolor);
  //terminates the newline
  fgetc(fp);
  //fread(plainbitmap,sizeof(uint8_t),height*width,fp);
  canvas *c = canvas_init(width,height);
  c->magic = magic;
   for(int i=0;i<width;i++){
    for(int j=0;j<height;j++){
	   fread(buff,sizeof(uint8_t),3,fp);
	   memcpy(canvas_get_pixel(c,i,j)->colors,buff, sizeof(uint8_t)*3);
	   print_pixel(canvas_get_pixel(c,i,j));
      }
#ifdef debug
      printf("\n");
#endif
   }
  return c;
}

/**
 * Schreibt ein Canvas in eine Datei
 * 
 * @param FILE* fp schreibbarer file pointer
 * @param canvas* canvas das in die Datei geschrieben wird
*/
void write_pbm(FILE *fp, canvas *c){
  int height = c->pixmap->height;
  int width = c->pixmap->width;
  fprintf(fp,"%s\n%d %d\n%d\n",c->magic,width,height,c->maxcolor);
  write_bitmap(fp,c->pixmap);
}