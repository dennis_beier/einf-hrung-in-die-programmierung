#include "operation.h"

int main(void){
  canvas *c = canvas_init(30,30);
  pixel color1 = {{199,104,104}};
  pixel color2 = {{75,250,75}};
  make_gradient(color1,color2,c);
  FILE *file = fopen("sample.pbm","w");
  write_pbm(file,c);
}