char* read_word(char **string, unsigned int size, FILE* file){
  //printf("read\n");
  const int max_buff = 2;
  char buff[max_buff];
  char *realloc_buff;
  //Der Rückgabewer wird nur nach Nullzeiger überprüft, deshalb cast nach int
  int return_value = (long int) fgets(buff,max_buff,file); 
  //size gibt an das wie oft sich die Funktion schon selbst aufgerufen hat bzw. das wievielfache von max_buff der String Groß ist
  if(size>1){
    //printf("realloc read buffer");
    
    realloc_buff = realloc(*(string),sizeof(char)*max_buff*size);
    //Falls kein Hauptspeicher mehr zur Verfügung steht gib einen Fehler Wert zurück, sonst wird der Puffer kopiert
    if(realloc_buff != NULL){
      *(string) = realloc_buff;
    }else{
      return NULL;
    }
    //An dem zweiten Durchlauf wird mehr Speicher für den String reserviert
  }
  //printf("%s\n",buff);
  
  //Wenn kein EOF ,Leerzeichen, oder Zeilenende gefunden wurde, lese die nächsten Zeichen ein
  if(return_value != 0 && !(strchr(buff,'\n')) && !(strchr(buff,' ')&&!(strchr(buff,EOF)))){
    strcat(*(string),buff);
    return read_word(string,size+1,file);  
  }else{
  //Sonst gebe Zeiger auf string zurück
    return *(string);
  }
}

char *read_word_file(FILE *file){
  char *string = calloc(1,sizeof(char));
  return read_word(&string,1,file);
}

//liest vom beginn bis zum ende n Zeichen aus einer Datei
char* read_n_chars(char* filename,long int begin, long int end){
  FILE *file = fopen(filename, "r");
  fseek(file,begin,SEEK_CUR);
  const int max_buff = 2;
  char buff[max_buff];
  char *string = calloc(1,sizeof(char)),*realloc_buff;
  int read_till_end = 0;
  //falls das Ende auf null gesetzt ist, wird bis zum Ende gelesen
  if(end == 0){
    read_till_end = 1;
    end = 100;
  }
  //schleife die den Cursor immer eins weiter rückt und ein Zeichen an den String anschließt
  for(int i = 1; i<=end;i++){
      //Der Rückgabewer wird nur nach Nullzeiger überprüft, deshalb cast nach int
      int return_value = (long int) fgets(buff,max_buff,file); 
      if(return_value != 0){
	realloc_buff = realloc(string,sizeof(char)*max_buff*i);
	if(realloc_buff != NULL){
	  string = realloc_buff;
	  strcat(string, buff);
	  if(read_till_end)
	    end++;
	}else{
	  return NULL;
	}
      }else{
	fclose(file);
	return string;
      }
  }
  fclose(file);
  return string;
}