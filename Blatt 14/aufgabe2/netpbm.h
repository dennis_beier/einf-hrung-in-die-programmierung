#ifndef __NETBPM_H__
#define __NETBPM_H__
#include <inttypes.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
  typedef struct pixel{
  uint8_t colors[3];
  } pixel;
  typedef pixel color;
  typedef struct bitmap {
	int width,height;
	pixel **map;
  }bitmap;

  typedef struct canvas{
	bitmap *pixmap;
	char *magic;
	int maxcolor;
  }canvas;
//Erzeigt eine Bitmap
bitmap *init_bitmap(int width, int height);

void add_pixel(pixel *pix1, pixel *pix2);

void mult_pixel(pixel *pix1, int i);  

void print_pixel(pixel *pix);
void make_gradient(color color1, color color2, canvas *c);

void write_pixel(FILE *file, pixel *pix);

void write_bitmap(FILE* file, bitmap *pixmap);

canvas *canvas_init(int width, int height);

void canvas_free(canvas *c);

pixel *canvas_get_pixel(canvas *c,int x, int y);

//Funktioniert nicht, vieleicht später fixxen
void convert_plainmap_to_bitmap(canvas *c, uint8_t *plainmap);


canvas *read_pbm(FILE *fp);



void write_pbm(FILE *fp, canvas *c);
#endif
  
