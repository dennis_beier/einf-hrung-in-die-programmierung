#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "operation.h"
#define debug
int main(int argc, char** argv){

  FILE *input = stdin,*output = stdout;
  //printf("%d\n", argc);
  //Überprüft die Nazhal der Parameter
  ////Bei nicht genügender Anzahl gibt es eine Fehlermeldung aus

  if(argc != 3){
    printf("Invalid Arguments!\n");
    printf("Usage: convert-operation input output!\n");
    exit(0);
  }
  //Falls das Argument nicht - ist, dann wird der übergebene Dateiname geöffnet
  if(strcmp("-",argv[1]) != 0){
      input = fopen(argv[1],"r");
      if(input == NULL){
	printf("Could not open file\nStop Execution\n");
	exit(EXIT_FAILURE);
      }
  }
  
  canvas *c = read_pbm(input);
  fclose(input);
  operation(c);
  //Falls das Argument nicht - ist, dann wird der übergebene Dateiname geöffnet
   if(strcmp("-",argv[2]) != 0){
      output = fopen(argv[2],"w");
      if(output == NULL){
	printf("Could not open file\n Stop Execution\n");
	exit(EXIT_FAILURE);
      }
  }
  write_pbm(output,c);
  canvas_free(c);
  fclose(output);
  exit(0);
}